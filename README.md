# Tutorials TK3 Journée Drones 2024

This project repository contains the tutorial material related to the Telekyb3 architecture used during the event `Journée Drones 2024`, which has been held at Rennes on Friday May 24th.

## Requirements

To run the tutorials, please build and run the docker image as described at this [link](https://gitlab.inria.fr/rainbow-public/telekyb3/telekyb3_docker/-/blob/main/README.md?ref_type=heads).

## Configuration

- Run the telekyb3 docker image
- In the docker terminal, type the following commands:

```bash
developer@host $ cd $DRONE_WS/tutorials_tk3_jd2024
```

Then, you can find the following folders:

- `doc` contains the markdown files composing the documentation of this project repository.
- `etc` contains extra material, like configuration scripts.
- `sim` contains the source code related to the simulation tutorials.

## Tutorials

- [A quad-rotor simulation in Gazebo](./doc/sim/quad_gazebo/quad_gazebo.md)
- [An hexa-rotor simulation in Gazebo](./doc/sim/hexa_gazebo/hexa_gazebo.md)
- [A fully-actuated hexa-rotor simulation in Gazebo](./doc/sim/fa_hexa_gazebo/fa_hexa_gazebo.md)