# Quad-rotor simulation in Gazebo

[[_TOC_]]

## 1. Introduction

In this tutorial, we will run a simulation of a quad-rotor within the Gazebo simulator.

As we will see later on, the simulation involves three parts:

1. the robot takes off from an initial position,
2. it follows a set of waypoints,
3. lastly, in the last reached waypoint, it lands and stop its motors.

> This tutorial is derived from the official tutorial [Running a quadrotor simulation](https://git.openrobots.org/projects/telekyb3/pages/software/run/quadrotor-simulation) which is available in the [Telekyb3 documentation](https://git.openrobots.org/projects/telekyb3/pages/index).

## 2. Set up the simulation

### 2.1. Middleware and Genom3 components

At this point, we start by launching the middleware, then all the required Genom3 components.

To do so, we can use the bash script `launch_genom_pocolibs.sh`.

**NB:** As you can guess from its filename, we are going to use `pocoLibs` as middleware within this tutorial.

Before running this script, we can inspect its content. Thus, in the terminal we type:

```bash
$ cat launch_genom_pocolibs.sh
#!/bin/bash

# Start pocolibs
h2 init

# Start genomix
genomixd &

# Launch Genom components for pocolibs
optitrack-pocolibs &
rotorcraft-pocolibs &
pom-pocolibs &
nhfc-pocolibs &
maneuver-pocolibs &
```

As we can see, the first command is `h2 init` which serves to initialize the middleware and to allow the communication between the components that we will run later on.

Then, we launch the `genomix` daemon [[1]](#ref1) which allows us to interact with the components, which will be running on the machine, from an external user application (e.g., a tcl, python or matlab script). Indeed, as we will see in the following, we will use some Python scripts to interact with the different components and proceed along the different simulation stages.
> **NB:** As we would like to use a Python script, then we will need to have **python-genomix** installed.
> Other possibilities are the use of a TCL or MATLAB script, which in turn require the installation of [tcl-genomix](https://git.openrobots.org/projects/tcl-genomix?jump=welcome) and [matlab-genomix](https://git.openrobots.org/projects/matlab-genomix), respectively.

At this point, we can run the different components that we will be using for the simulation, namely `optitrack`, `rotorcraft`, `pom`, `nhfc` and `maneuver`.

As we are using `pocoLibs` as middleware, we need to specify this choice when launching the binaries of the components we want to run. Hence, if we consider the case of `optitrack`, to run this component for `pocoLibs`, we need to run the command `optitrack-pocolibs`. We do similarly for the remaining components, thus `rotorcraft-pocolibs`, `pom-pocolibs`, `nhfc-pocolibs`, and `maneuver-pocolibs`.
As we can see, the last lines of `launch_genomix_pocolibs.sh` are indeed these instructions.

Now that we have understood the content of the bash script, we can run it by typing the following commands:

```bash
$ chmod +x launch_genom_pocolibs.sh
$ ./launch_genom_pocolibs.sh
Initializing pocolibs devices: OK
optitrack: created outport optitrack/genom_state
optitrack: created outport optitrack/genom_metadata
optitrack: spawned task publish
optitrack: spawned control task
optitrack: setup and running
nhfc: created outport nhfc/genom_state
nhfc: created outport nhfc/genom_metadata
pom: created outport pom/genom_state
maneuver: created outport maneuver/genom_state
rotorcraft: created outport rotorcraft/genom_state
nhfc: created outport nhfc/rotor_input
pom: created outport pom/genom_metadata
nhfc: created inport nhfc/rotor_measure
nhfc: created inport nhfc/state
nhfc: created inport nhfc/reference
pom: spawned task io
maneuver: created outport maneuver/genom_metadata
rotorcraft: created outport rotorcraft/genom_metadata
maneuver: created inport maneuver/state
rotorcraft: created inport rotorcraft/rotor_input
nhfc: created outport nhfc/wrench_measure
maneuver: created outport maneuver/desired
rotorcraft: created outport rotorcraft/rotor_measure
nhfc: spawned task main
rotorcraft: created outport rotorcraft/imu
maneuver: created outport maneuver/horizon
rotorcraft: created outport rotorcraft/mag
rotorcraft: spawned task main
maneuver: spawned task plan
maneuver: spawned task exec
pom: spawned task filter
nhfc: spawned control task
nhfc: setup and running
rotorcraft: spawned task comm
rotorcraft: spawned control task
rotorcraft: setup and running
pom: created outport pom/frame/robot
pom: spawned control task
pom: setup and running
maneuver: spawned control task
maneuver: setup and running
```

From the output of the terminal, we can see that after initializing `pocoLibs`, the different components have been launched. Now, their associated processes are currently running in the background of our machine.

### 2.2. Gazebo simulator

At this point, we can run the simulator `Gazebo`, which would allow simulating the robot we will control.

An example world file is provided within the default installation of the Telekyb3 software architecture.
This example world file is available at `$ROBOTPKG_BASE/share/gazebo/worlds/example.world`.

Then, to run it, it is sufficient to open another terminal, and type the following command:

```bash
$ gazebo --verbose $ROBOTPKG_BASE/share/gazebo/worlds/example.world
```

The simulator should start and provide you a scene like the one shown in the figure below.

![gazebo_world](./imgs/gazebo_world.jpg)

There, we can see two robots: a (collinear-propellers) quad-rotor on the left, and an (tilted-propellers) hexa-rotor on the right.

**In this tutorial, we will control the quad-rotor on the left.**
> We will control the hexa-rotor in the [next one](../hexa_gazebo/hexa_gazebo.md).

In the terminal where we have launched the Gazebo simulator, we should have an output similar to the one shown below.

```bash
Gazebo multi-robot simulator, version 11.10.2
Copyright (C) 2012 Open Source Robotics Foundation.
Released under the Apache 2 License.
http://gazebosim.org

[Msg] Waiting for master.
Gazebo multi-robot simulator, version 11.10.2
Copyright (C) 2012 Open Source Robotics Foundation.
Released under the Apache 2 License.
http://gazebosim.org

[Msg] Waiting for master.
[Msg] Connected to gazebo master @ http://127.0.0.1:11345
[Msg] Publicized address: 131.254.160.38
[Msg] Loading world file [<path_to_openrobots>/openrobots/share/gazebo/worlds/example.world]
gzserver: optitrack publishing HR_6
gzserver: optitrack publishing QR_4
gzserver: optitrack listening on port 1509
gzserver: mrsim created /tmp/pty-hr6
gzserver: mrsim created /tmp/pty-qr4
[Msg] Connected to gazebo master @ http://127.0.0.1:11345
[Msg] Publicized address: 131.254.160.38
[Msg] Warning: Ignoring XDG_SESSION_TYPE=wayland on Gnome. Use QT_QPA_PLATFORM=wayland to run on Wayland anyway.
[Wrn] [Event.cc:61] Warning: Deleting a connection right after creation. Make sure to save the ConnectionPtr from a Connect call
```

As we can see from the line

```bash
[Msg] Loading world file [<path_to_openrobots>/openrobots/share/gazebo/worlds/example.world]
```

Gazebo is informing us of which world file is loading. In this case, you should have that `<path_to_openrobots>` matches the content of your environment variable `ROBOTPKG_BASE`.

In the output above, the interesting lines for the sake of the simulation are the following ones:

```bash
gzserver: optitrack publishing HR_6
gzserver: optitrack publishing QR_4
gzserver: optitrack listening on port 1509
gzserver: mrsim created /tmp/pty-hr6
gzserver: mrsim created /tmp/pty-qr4
```

As we can infer from them, we have an `optitrack` plugin running and publishing the state of two robots: the state of one robot is published with the name `HR_6`, while the other one as `QR_4`. The former (`HR_6`) denotes the hexa-rotor in the scene, while the latter (`QR_4`) the quad-rotor.
Here, the Gazebo world file contains the istantiation of the plugin [optitrack-gazebo](https://git.openrobots.org/projects/optitrack-gazebo), which simulates an [Optitrack Motion Capture system](https://optitrack.com/).

Then, we can also notice the plugin `mrsim`, which has created two connections: one at `/tmp/pty-hr6`, and a second one at `/tmp/pty-qr4`. Similarly to the previous case, the connection `/tmp/pty-hr6` is the one for the hexa-rotor, while `/tmp/pty-qr4` the one related to the quad-rotor. Here, the plugin [mrsim-gazebo](https://git.openrobots.org/projects/mrsim-gazebo) is used to simulate the Telekyb3 low-level hardware (flight controller and ESCs) of a generic multi-rotor aerial.

As in this tutorial we will work with the quad-rotor, we will make use of the strings `QR_4` and `/tmp/pty-qr4` to, respectively, access the quad-rotor state from the motion capture, and interact with its simulated low-level hardware (e.g., to send the rotor commands and read the telemetry - IMU - data).

### 2.3. Python script

Now, we have our _simulated_ robot operative and spawned in the Gazebo simulator, as well as all the necessary Genom3 components running on your machine.

However, the Genom3 components are still in an _idle_ state and they are not performing any action. In fact, the robot is neither moving nor performing any other task.

The reason is because the Genom3 components used to control the robot (`optitrack`, `rotorcraft`, ...) are still not properly configured and connected to each other. Indeed, any Genom3 component normally requires a set of parameters for being properly configured (e.g., according to the type of robot that is controlled) and to access some data from some input ports.

**NB:** The setting of the parameters and the port connections are tasks that an user shall perform from an external application.

For this scope, there exists [genomix interface](https://git.openrobots.org/projects/genomix), whose purpose is indeed to allow an user to interact with the middleware and the Genom3 components from an external application.
Therefore, an user can write a script employing this `genomix` interface to 1) initialize the different components, 2) connect their ports, and 3) set the proper parameters related to our robot and simulation environment (e.g., the robot mass and inertia, the magnetic field intensity, ...).

In this tutotial, we will perform these actions in a Python script by employing the `python-genomix` interface, which is the implementation of `genomix` for the Python language.
> Currently, the `genomix` interface has been implemented for the TCL ([tcl-genomix](https://git.openrobots.org/projects/tcl-genomix?jump=welcome)) or MATLAB ([matlab-genomix](https://git.openrobots.org/projects/matlab-genomix)) programming languages.

A script using [python-genomix](https://git.openrobots.org/projects/python-genomix) is already available within the material of this tutorial, and it is named `sim_quad.py`.

Lets have a look to the content of the Python script `sim_quad.py`, which is reported below for convenience.

```python
################ LIBRARY IMPORT
import genomix
import os


################ INITIALIZATION
# this connects to components running on the same host (localhost)
g = genomix.connect()
# to instead control components running on the remote computer "hostname" use
# g = genomix.connect('hostname')

# adapt path to your setup
g.rpath(os.environ['ROBOTPKG_BASE'] + '/lib/genom/pocolibs/plugins')

# load components clients
optitrack = g.load('optitrack') # motion capture
rotorcraft = g.load('rotorcraft') # robot hardware interface
pom = g.load('pom') # state estimator
nhfc = g.load('nhfc') # attitude-position controller
maneuver = g.load('maneuver') # motion planner


################ FUNCTIONS
# --- setup ----------------------------------------------------------------
#
# Configure the Genom3 components.
# To be called once.
# To be called interactively.
def setup():
  ######## optitrack
  #
  # connect to the simulated optitrack system on localhost
  optitrack.connect({
    'host': 'localhost', 'host_port': '1509', 'mcast': '', 'mcast_port': '0'
  })

  ######## rotorcraft
  #
  # connect to the simulated quadrotor
  rotorcraft.connect({'serial': '/tmp/pty-qr4', 'baud': 0})
  #
  # get IMU at 1kHz and motor data at 20Hz
  rotorcraft.set_sensor_rate({'rate': {
    'imu': 1000, 'mag': 0, 'motor': 20, 'battery': 1
  }})
  #
  # Filter IMU: 20Hz cut-off frequency for gyroscopes and 5Hz for
  # accelerometers. This is important for cancelling vibrations.
  rotorcraft.set_imu_filter({
    'gfc': [20, 20, 20], 'afc': [5, 5, 5], 'mfc': [20, 20, 20]
  })
  #
  # read propellers velocities from nhfc controller
  rotorcraft.connect_port({
    'local': 'rotor_input', 'remote': 'nhfc/rotor_input'
  })

  ######## pom
  #
  # configure kalman filter
  pom.set_prediction_model('::pom::constant_acceleration')
  pom.set_process_noise({'max_jerk': 100, 'max_dw': 50})
  #
  # allow sensor data up to 250ms old
  pom.set_history_length({'history_length': 0.25})
  #
  # configure magnetic field
  pom.set_mag_field({'magdir': {
    'x': 23.8e-06, 'y': -0.4e-06, 'z': -39.8e-06
  }})
  #
  # read IMU and magnetometers from rotorcraft
  pom.connect_port({'local': 'measure/imu', 'remote': 'rotorcraft/imu'})
  pom.add_measurement('imu')
  pom.connect_port({'local': 'measure/mag', 'remote': 'rotorcraft/mag'})
  pom.add_measurement('mag')
  #
  # read position and orientation from optitrack
  pom.connect_port({
    'local': 'measure/mocap', 'remote': 'optitrack/bodies/QR_4'
  })
  pom.add_measurement('mocap')

  ######## nhfc
  #
  # PID tuning
  nhfc.set_saturation({'sat': {'x': 1, 'v': 1, 'ix': 0}})
  nhfc.set_servo_gain({ 'gain': {
      'Kpxy': 5, 'Kpz': 5, 'Kqxy': 4, 'Kqz': 4,
      'Kvxy': 6, 'Kvz': 6, 'Kwxy': 1, 'Kwz': 1,
      'Kixy': 0, 'Kiz': 0
  }})
  #
  # use full-attitude controller
  nhfc.set_control_mode({'att_mode': '::nhfc::full_attitude'})
  # set minimum and maximum propeller velocities
  nhfc.set_wlimit({
    'wmin': 16, 'wmax': 100
  })
  #
  # configure quadrotor geometry: 4 rotors, not tilted, 23cm arms
  nhfc.set_gtmrp_geom({
      'rotors': 4, 'cx': 0, 'cy': 0, 'cz': 0, 'armlen': 0.23, 'mass': 1.28,
      'rx':0, 'ry': 0, 'rz': -1, 'cf': 6.5e-4, 'ct': 1e-5
  })
  #
  # emergency descent parameters
  nhfc.set_emerg({'emerg': {
      'descent': 0.1, 'dx': 0.5, 'dq': 1, 'dv': 3, 'dw': 3
  }})
  #
  # read measured propeller velocities from rotorcraft
  nhfc.connect_port({
      'local': 'rotor_measure', 'remote': 'rotorcraft/rotor_measure'
  })
  #
  # read current state from pom
  nhfc.connect_port({
      'local': 'state', 'remote': 'pom/frame/robot'
  })
  #
  # read reference trajectory from maneuver
  nhfc.connect_port({
      'local': 'reference', 'remote': 'maneuver/desired'
  })

  ######## maneuver
  #
  # set planning bounds
  pi = 3.14159265358
  maneuver.set_bounds({
    'xmin': -10, 'xmax': +10, 'ymin': -10, 'ymax': +10, 
    'zmin': 0, 'zmax': +10, 'yawmin': -2*pi, 'yawmax': +2*pi
  })
  # configure kalman filter
  maneuver.connect_port({
    'local': 'state', 'remote': 'pom/frame/robot'
  })


# --- start ----------------------------------------------------------------
#
# Spin the motors and servo on current position. 
# To be called interactively.
def start():
  # Start logging the components
  optitrack.set_logfile('/tmp/optitrack.log')
  rotorcraft.log('/tmp/rotorcraft.log')
  pom.log_state('/tmp/pom.log')
  pom.log_measurements('/tmp/pom-measurements.log')
  nhfc.log('/tmp/nhfc.log')
  maneuver.log('/tmp/maneuver.log')
  
  rotorcraft.start() # start spinning the motors
  rotorcraft.servo(ack=True) # this runs until stopped or input error
  nhfc.set_current_position() # hover on current position


# --- takeoff --------------------------------------------------------------
#
# Enable motion planner and start 5s taking-off motion up to 1m height.
# To be called interactively.
def takeoff():
  maneuver.set_current_state() # set initial planning position to current one
  maneuver.take_off(1.0, 3, ack=True) # this runs until the trajectory has been fully generated
  nhfc.servo(ack=True) # this runs untill stopped or input error


# --- square_trajectory ----------------------------------------------------
#
# Generate a square-shaped trajectory made of 4 waypoints.
# To be called interactively.
def square_trajectory():
  pi=3.141592653589
  maneuver.goto(1,0,1,pi/2,0,ack=True) # move to waypoint
  maneuver.waypoint(1,2,1,pi,0,0,0,0,0,0,0,0,ack=True) # push a waypoint to be reached after the last one
  maneuver.waypoint(-1,2,1,1.5*pi,0,0,0,0,0,0,0,0,ack=True)
  maneuver.waypoint(-1,0,1,2*pi,0,0,0,0,0,0,0,0,ack=True)


# --- land -----------------------------------------------------------------
#
# Land to ground in 5s at the current position
# To be called interactively.
def land():
  maneuver.take_off(0.05, 3, ack=True) # this runs until the trajectory has been fully generated


# --- stop -----------------------------------------------------------------
#
# Stop motors. To be called interactively
# To be called interactively.
def stop():
  # Stop the robot
  maneuver.stop() # stop any motion
  nhfc.stop() # stop tracking any reference position and attitude
  rotorcraft.stop() # stop motors
  
  # Stop logging the components
  optitrack.unset_logfile()
  rotorcraft.log_stop()
  pom.log_stop()
  nhfc.log_stop()
  maneuver.log_stop()
```

As we can see, after importing the required libraries, we have an initialization part and then a definition of a set of functions.

Lets have a look at the part where we import the required python libraries:

```python
################ LIBRARY IMPORT
import genomix
import os
```

there, we can see that we are indeed importing the library `genomix`. If properly installed, it can be imported as any other Python library.

Now, lets analyze the initialization part, i.e.:

```python
################ INITIALIZATION
# this connects to components running on the same host (localhost)
g = genomix.connect()
# to instead control components running on the remote computer "hostname" use
# g = genomix.connect('hostname')

# adapt path to your setup
g.rpath(os.environ['ROBOTPKG_BASE'] + '/lib/genom/pocolibs/plugins')

# load components clients
optitrack = g.load('optitrack') # motion capture
rotorcraft = g.load('rotorcraft') # robot hardware interface
pom = g.load('pom') # state estimator
nhfc = g.load('nhfc') # attitude-position controller
maneuver = g.load('maneuver') # motion planner
```

There, we can see that:

- We first connect to the genomix demon server running in localhost.
 By default, when running the `connect` function with no arguments, the client application connects to a server in localhost. Thus, it would be equivalent to `g = genomix.connect('localhost')`. For more details about its usage, please refer the [documentation of python-genomix](https://git.openrobots.org/projects/python-genomix/pages/README).
 This connection would provide us with an handle that will allow us to connect to each Genom3 component.
- Next, we load the definitions related to all the Genom3 components that we have installed within the `ROBOTPKG_BASE` path. Since we use `pocoLibs` as target middleware, we need to load the interface definitions for this middleware, thus we need to add to the base path the suffix `/lib/genom/pocolibs/plugins`. This step is very important because we have to load, for each component, the information about its callable services and the data structures passed as arguments and received in return. Otherwise, the client would not know how to interface with a choosen Genom3 component.
- After _"loading"_ the components, we will obtain an handle for each running Genom3 component, which will be used later on to interact with it and call its services.

Before inspecting the last part of the Python script, we can now run it. Thus, in another terminal, we can execute interactively our python script [[2]](#ref2) by typing

```bash
$ python3 -i sim_quad.py
```

This will run the first parts of the code we saw earlier, but no functions, as the latter ones are meant to be called manually.

At this point, we are connected to the local instance of the genomix deamon server and we have an handle to each Genom3 component running on our machine.

As we will see more in detail in the next section, the rest of the Python script is a definition of functions which be used to run the simulation and progress through its stages.

## 3. Run the simulation

Hereafter, we will understand how to perform the simulation and fly our quad-rotor in the Gazebo simulator.

### 3.1. Set up the quad-rotor

We start by setting up the software components in charge of controlling our robot.
To do that, in the code, we can find the function `setup`, which contains the following lines of code:

```python
# --- setup ----------------------------------------------------------------
#
# Configure the Genom3 components.
# To be called interactively.
def setup():
  ######## optitrack
  #
  # connect to the simulated optitrack system on localhost
  optitrack.connect({
    'host': 'localhost', 'host_port': '1509', 'mcast': '', 'mcast_port': '0'
  })

  ######## rotorcraft
  #
  # connect to the simulated quadrotor
  rotorcraft.connect({'serial': '/tmp/pty-qr4', 'baud': 0})
  #
  # get IMU at 1kHz and motor data at 20Hz
  rotorcraft.set_sensor_rate({'rate': {
    'imu': 1000, 'mag': 0, 'motor': 20, 'battery': 1
  }})
  #
  # Filter IMU: 20Hz cut-off frequency for gyroscopes and 5Hz for
  # accelerometers. This is important for cancelling vibrations.
  rotorcraft.set_imu_filter({
    'gfc': [20, 20, 20], 'afc': [5, 5, 5], 'mfc': [20, 20, 20]
  })
  #
  # read propellers velocities from nhfc controller
  rotorcraft.connect_port({
    'local': 'rotor_input', 'remote': 'nhfc/rotor_input'
  })

  ######## pom
  #
  # configure kalman filter
  pom.set_prediction_model('::pom::constant_acceleration')
  pom.set_process_noise({'max_jerk': 100, 'max_dw': 50})
  #
  # allow sensor data up to 250ms old
  pom.set_history_length({'history_length': 0.25})
  #
  # configure magnetic field
  pom.set_mag_field({'magdir': {
    'x': 23.8e-06, 'y': -0.4e-06, 'z': -39.8e-06
  }})
  #
  # read IMU and magnetometers from rotorcraft
  pom.connect_port({'local': 'measure/imu', 'remote': 'rotorcraft/imu'})
  pom.add_measurement('imu')
  pom.connect_port({'local': 'measure/mag', 'remote': 'rotorcraft/mag'})
  pom.add_measurement('mag')
  #
  # read position and orientation from optitrack
  pom.connect_port({
    'local': 'measure/mocap', 'remote': 'optitrack/bodies/QR_4'
  })
  pom.add_measurement('mocap')

  ######## nhfc
  #
  # PID tuning
  nhfc.set_saturation({'sat': {'x': 1, 'v': 1, 'ix': 0}})
  nhfc.set_servo_gain({ 'gain': {
      'Kpxy': 5, 'Kpz': 5, 'Kqxy': 4, 'Kqz': 4,
      'Kvxy': 6, 'Kvz': 6, 'Kwxy': 1, 'Kwz': 1,
      'Kixy': 0, 'Kiz': 0
  }})
  #
  # use full-attitude controller
  nhfc.set_control_mode({'att_mode': '::nhfc::full_attitude'})
  # set minimum and maximum propeller velocities
  nhfc.set_wlimit({
    'wmin': 16, 'wmax': 100
  })
  #
  # configure quadrotor geometry: 4 rotors, not tilted, 23cm arms
  nhfc.set_gtmrp_geom({
      'rotors': 4, 'cx': 0, 'cy': 0, 'cz': 0, 'armlen': 0.23, 'mass': 1.28,
      'rx':0, 'ry': 0, 'rz': -1, 'cf': 6.5e-4, 'ct': 1e-5
  })
  #
  # emergency descent parameters
  nhfc.set_emerg({'emerg': {
      'descent': 0.1, 'dx': 0.5, 'dq': 1, 'dv': 3, 'dw': 3
  }})
  #
  # read measured propeller velocities from rotorcraft
  nhfc.connect_port({
      'local': 'rotor_measure', 'remote': 'rotorcraft/rotor_measure'
  })
  #
  # read current state from pom
  nhfc.connect_port({
      'local': 'state', 'remote': 'pom/frame/robot'
  })
  #
  # read reference trajectory from maneuver
  nhfc.connect_port({
      'local': 'reference', 'remote': 'maneuver/desired'
  })

  ######## maneuver
  #
  # set planning bounds
  pi = 3.14159265358
  maneuver.set_bounds({
    'xmin': -10, 'xmax': +10, 'ymin': -10, 'ymax': +10, 
    'zmin': 0, 'zmax': +10, 'yawmin': -2*pi, 'yawmax': +2*pi
  })
  # configure kalman filter
  maneuver.connect_port({
    'local': 'state', 'remote': 'pom/frame/robot'
  })
```

As we can see, for each component, we use the handles to call a set of services.

All these services are meant to initialize the components, connect their ports, and set the necessary parameters of our robot and simulation environment.

For instance, the line

```python
optitrack.connect({
    'host': 'localhost', 'host_port': '1509', 'mcast': '', 'mcast_port': '0'
  })
```

allows to connect to the optitrack plugin running within the Gazebo simulator (i.e., `optitrack-gazebo`).

Then, we can find a section related to the Genom3 component `rotorcraft`. A very important line is the following one:

```python
rotorcraft.connect({'serial': '/tmp/pty-qr4', 'baud': 0})
```

which allows to connect to the Telekyb3 simulated low-level hardware of our quad-rotor (i.e. the one simulated by the plugin `mrsim-gazebo`).

**NB:** the component `rotorcraft` is indeed the interface to the Telekyb3 flight hardware.

In particular, this line instructs the component `rotorcraft` to connect to the simulated flight controller, which is connected to the simulated port `/tmp/pty-qr4` opened by the plugin `mrsim-gazebo`. This simulated connection resembles the real USB port made available by the real flight controller once connected by USB on the real robot's onboard computer.
Next, we set the parameters related to the IMU (accelerometer, gyroscope), the magnetometer and the motor telemetry: precisely, the polling rates of the sensors and the cut-off frequencies of the sensor filters. These actions are performed by the lines:

```python
#
# get IMU at 1kHz and motor data at 20Hz
rotorcraft.set_sensor_rate({'rate': {
  'imu': 1000, 'mag': 0, 'motor': 20, 'battery': 1
}})
#
# Filter IMU: 20Hz cut-off frequency for gyroscopes and 5Hz for
# accelerometers. This is important for cancelling vibrations.
rotorcraft.set_imu_filter({
  'gfc': [20, 20, 20], 'afc': [5, 5, 5], 'mfc': [20, 20, 20]
})
```

Lastly, for the Genom3 component `rotorcraft`, we connect the input ports to the outputs of some other components. In this case, we have the lines:

```python
rotorcraft.connect_port({
    'local': 'rotor_input', 'remote': 'nhfc/rotor_input'
})
```

The component `rotorcraft` requires only 1 input, which shall be fed to its input port `rotor_input` (here denoted as `local` in the python dictionary). As we can see from the line of code above, the input port `rotor_input` of `rotorcraft` is connected to the output port `rotor_input` of `nhfc`. As that output port belongs to another component (`nhfc`), we need to add the prefix `nhfc/` to its name when calling the service `connect_port`. This also motivates the use of the dictionary field name `remote` to denote this port.

**NB:** In Telekyb3, in most cases, the names of the ports suggest also how they should be connected. For instance, the `rotorcraft` inpur port `rotor_input` is connected to the output port `rotor_input` of `nhfc`. Similarly, for other port names. However, in some other cases, this may not be true.

For all the other components, a similar workflow is undertaken, which can be summerized as follows:

- If necessary, use the component handle to connect to the simulated (or real) hardware, sensor or device that the Genom3 component is meant to connect to and interface with.
- Set the simulation (or experiment) and the robot parameters, using the related services (usually the are named as `set_...`).
- Connect the input ports of each component to the output ports of the other components providing the data that the former requires.

**NB:** It is important to keep in mind that, for each component, the documentation related to its services and ports can be found at <https://git.openrobots.org/>, by looking for the component project. For instance, if we need the documentation of `rotorcraft`, we need to type in the search bar `rotorcraft-genom3` and then look for the `Documentation` tab, leading us to its documentation page.
> For instance, the documentation of `rotorcraft-genom3` can be found at the link <https://git.openrobots.org/projects/rotorcraft-genom3/pages/README>.

At this point, we can launch the function `setup`. To do that, it is sufficient to type the following command in the Python interpreter command view and press ENTER in your keyboard:

```python
>>> setup()
```

Now, looking at the terminal where we launched the bash script `launch_genom_pocolibs.sh`, we will see the following output:

```bash
optitrack: created outport optitrack/bodies/HR_6
optitrack: created outport optitrack/bodies/QR_4
rotorcraft-pocolibs: connected to chimera1.1, /tmp/pty-qr4
pom: created inport pom/measure/imu
pom: created inport pom/measure/mag
pom: created inport pom/measure/mocap
```

As we connected to the simulated motion capture system, the Genom3 component `optitrack` is now creating two output ports, one for each body.
In the `setup` function of the Python script, we can indeed see that we used that port in the port connections of the component `pom`.

```python
# read position and orientation from optitrack
pom.connect_port({
  'local': 'measure/mocap', 'remote': 'optitrack/bodies/QR_4'
})
```

Similarly, the component `pom` has created three input ports, one for each sensor input (i.e. `imu` for the IMU, `mag` for the magnetometer, and `mocap` for the motion capture), after calling the `add_measurement` service.
Indeed, in the function `setup`, we can see these 3 lines of code:

```python
pom.add_measurement('imu')
pom.add_measurement('mag')
pom.add_measurement('mocap')
```

which are interposed by these other ones for the related port connections:

```python
pom.connect_port({'local': 'measure/imu', 'remote': 'rotorcraft/imu'})
pom.connect_port({'local': 'measure/mag', 'remote': 'rotorcraft/mag'})
#
# read position and orientation from optitrack
pom.connect_port({
  'local': 'measure/mocap', 'remote': 'optitrack/bodies/QR_4'
})
```

which connect `pom` to the output ports `imu` and `mag` of `rotorcraft` (providing the IMU and the magnetometer data), and the output port `bodies/QR_4` of `optitrack` (providing the position and attitude of the quad-rotor acquired by the motion capture).

### 3.2. Start and Take Off

Now, the Genom3 components are initialized, properly configured and well connected to each other.

It is time to take off and move our robot!

We can start the robot by running the `start` function.

After running this function, the rotors of the robot should start spinning faster and faster, and the quad-rotor will then start hovering at the initial location.

Here, the content of the function `start`:

```python
# --- start ----------------------------------------------------------------
#
# Spin the motors and servo on current position. 
# To be called interactively.
def start():
  # Start logging the components
  optitrack.set_logfile('/tmp/optitrack.log')
  rotorcraft.log('/tmp/rotorcraft.log')
  pom.log_state('/tmp/pom.log')
  pom.log_measurements('/tmp/pom-measurements.log')
  nhfc.log('/tmp/nhfc.log')
  maneuver.log('/tmp/maneuver.log')
  
  rotorcraft.start() # start spinning the motors
  rotorcraft.servo(ack=True) # this runs until stopped or input error
  nhfc.set_current_position() # hover on current position
```

In this function, we first instruct every Genom3 component to start logging, by passing the path and the filename where to save the log file filled the logged data.

> **NB:** Usually, the string provided to the logging services are either absolute paths or relative ones. The relative paths are _relative_ to the component and the directory where its executable has been launched. If we were considering this tutorial, it would have been relative to the path where we launched the bash script `launch_genom_pocolibs.sh`. However, in this tutorial, we used absolute paths for simplicity.

In the remaining part of the function `start`, we launch the `start` activity of `rotorcraft` and then the `servo`. The former activity (`start`) instructs the flight controller to start spinning the propellers at the minimum controllable speed. The latter activity (`servo`) tells the component `rotorcraft` to wait and execute the propeller commands which will be available in its input port, whose data are those coming from the output port `rotor_input` of `nhfc`.

**NB:** In the Telekyb3 _jargon_, the `servo` activities are those that are used to inform a given Genom3 component to _"listen"_ to the data incoming to its input ports, and use that data to perform their task. For instance, if we consider a controller, the `servo` activity will tell the component to listen to the input trajectory and _servo_ to it, i.e. move the robot according to the input motion.

To let the quad-rotor take off, we can run the function `takeoff`, which should let our robot move vertically up to 1m height.

If we inspect the content of this function, we will see the following lines of code:

```python
# --- takeoff --------------------------------------------------------------
#
# Enable motion planner and start 5s taking-off motion up to 1m height.
# To be called interactively.
def takeoff():
  maneuver.set_current_state() # set initial planning position to current one
  maneuver.take_off(1.0, 3, ack=True) # this runs until the trajectory has been fully generated
  nhfc.servo(ack=True) # this runs untill stopped or input error
```

The first line is used to instruct `maneuver` to plan the trajectory starting from the current robot state. Then, we trigger the generation of the take-off trajectory, thus `maneuver` will start writing to its output port the desired motion. Then, we tell `nhfc` to start the `servo` activity. From this moment on, `nhfc` will start reading the reference trajectory available to its input port (`desired`), and coming from `maneuver`, and generate proper motor commands to move the robot accordingly.

Once launched the `takeoff` function, the robot should move vertically as shown in the image below.

![gazebo_quad_takeoff](./imgs/gazebo_quad_takeoff.jpg)

### 3.3. Waypoint sequence

At this point, we can ask the robot to reach a desired waypoint by means of the component `maneuver`.

In the provided Python script, the function `square_trajectory` showcases a demo scenarios.
In fact, this function instructs `maneuver` to generate a trajectory across 4 waypoints forming a square.

After calling this function in the Python interpreter command-line view, the robot should start moving in the environment, as shown in the next figure.

![gazebo_quad_trajectory](./imgs/gazebo_quad_trajectory.jpg)

If we inspect the content of the function `square_trajectory`, which is reported below for convenience, we can see that it calls the services `goto` and `waypoint` of `maneuver`.
These two services allow adding 4 waypoints within the motion planning algorithm implemented by `maneuver`, and generate a trajectory passing through each one of them.

```python
# --- square_trajectory ----------------------------------------------------
#
# Generate a square-shaped trajectory made of 4 waypoints.
# To be called interactively.
def square_trajectory():
  pi=3.141592653589
  maneuver.goto(1,0,1,pi/2,0,ack=True) # move to waypoint
  maneuver.waypoint(1,2,1,pi,0,0,0,0,0,0,0,0,ack=True) # push a waypoint to be reached after the last one
  maneuver.waypoint(-1,2,1,1.5*pi,0,0,0,0,0,0,0,0,ack=True)
  maneuver.waypoint(-1,0,1,2*pi,0,0,0,0,0,0,0,0,ack=True)
```

### 3.4. Land and Stop

After completing the trajectory, the robot should have returned to the starting location, and there stop moving.

Now, we can make the robot land by using the function `land`, whose content is shown below.

```python
# --- land -----------------------------------------------------------------
#
# Land to ground in 5s at the current position
# To be called interactively.
def land():
  maneuver.take_off(0.05, 3, ack=True) # this runs until the trajectory has been fully generated
```

This function calls again the `take_off` service of `maneuver`, which can be conveniently used to generate vertical motions from the current robot position and attitude.

Once the robot is on the ground, we can stop its motors by running the function `stop`.
Its content is as follows:

```python
# --- stop -----------------------------------------------------------------
#
# Stop motors. To be called interactively
# To be called interactively.
def stop():
  # Stop the robot
  maneuver.stop() # stop any motion
  nhfc.stop() # stop tracking any reference position and attitude
  rotorcraft.stop() # stop motors
  
  # Stop logging the components
  optitrack.unset_logfile()
  rotorcraft.log_stop()
  pom.log_stop()
  nhfc.log_stop()
  maneuver.log_stop()
```

Here, we do the opposite of what we did within the `start` function: we fist stop the components in charge of the robot motion (`maneuver`, `nhfc` and `rotorcraft`), then we instruct all the logging components to stop writing the data in the log files.

### 3.5. Move the robot to other waypoints

Once the robot has taken off, it is possible to move it to other waypoints to the ones of the function `square_trajectory`.

As we have an handle to each component, we can use them to interact with them. In particular, we can use the one of `maneuver` to call the service `goto`, which allows to specify another waypoint.

```python
  maneuver.goto({
    x=<?> , y=<?>, z=<?>, yaw=<?>, duration=<?>, ack=True
  })
```

where `<?>` shall be replaced.

**NB:** If the robot does not move, it means that either the requested motion is outside the bounds set for `maneuver` (see service `set_bounds`) or it is unfeasible with the specified `duration` argument. If the latter case is occuring, try to change the `duration` argument of the service call `goto` with a larger value, or set it to `0` for a minimum-time trajectory.

#### Example of manual waypoint request

```python
# if not already done, run 
> setup() # calling this function multiple times is not a problem
> start() # propellers should start spinning faster and faster
> takeoff() # robot starts moving upwards
# wait for the robot to at the take-off height, then run
> maneuver.goto(5, 5, 2, 3.14, 0, ack=True)
# this will produce a minimum-time motion to the desired waypoint
# once the waypoint is reached, try running
> maneuver.goto(-1, 0, 1, 0, 0, ack=True)
# this will make the robot go back to the starting location
# once there, run
> land()
# wait for landing, then run
> stop()
```

### 3.6. Play with the other Genom3 components

You can play with the other components by using the available handles and their services.

Please, refer again to the documentation of each Genom3 component available within the <https://git.openrobots.org> project to know which services could be called, what they do, and which parameters they take as input arguments and what they return in output.

### 3.7. Kill the Genom3 components and stop middleware

In order to stop the Genom3 components and the middleware (in this case, `pocoLibs`), a bash script is provided, namely `kill_genom_pocolibs.sh`.

If we look at its content, we will see the following lines of code:

```bash
#!/bin/bash

# Kill the processes related to the Genom components
pkill optitrack-pocolibs &
pkill rotorcraft-pocolibs &
pkill pom-pocolibs &
pkill nhfc-pocolibs &
pkill maneuver-pocolibs &

# Kill genomix
pkill genomixd &

# Close pocolibs
h2 end
```

Here, we first kill any process related to the Genom3 components we started earlier. Then, we kill the genomix deamon server, and lastly we terminate the middleware by running the command `h2 end`.

To run this script, it is sufficient to type the following command in a terminal

```bash
$ ./kill_genom_pocolibs.sh
```

## 4. References

[1] [Daemon (computing)](https://en.wikipedia.org/wiki/Daemon_(computing)).<a name="ref1"></a>
[2] [Python interpreter in interactive mode](https://docs.python.org/3/tutorial/interpreter.html#interactive-mode). <a name="ref2"></a>

## 5. What is next?

In the next tutorial, we will fly with the hexa-rotor already present in the Gazebo world file used here.

Please, refer to [Hexa-rotor simulation in Gazebo](./../hexa_gazebo/hexa_gazebo.md) for the next tutorial.
