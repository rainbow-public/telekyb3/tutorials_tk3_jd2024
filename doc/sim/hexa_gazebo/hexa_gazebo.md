# Hexa-rotor simulation in Gazebo

[[_TOC_]]

## 1. Introduction

In this tutorial, we will run a simulation of an hexa-rotor within the Gazebo simulator.

Similarly to the [previous tutorial](./../quad_gazebo/quad_gazebo.md), the simulation involves the same three following parts:

1. the robot takes off from an initial position,
2. it follows a set of waypoints,
3. lastly, in the last reached waypoint, it lands and stop its motors.

## 2. Set up the simulation

### 2.1. Middleware and Genom3 components

At this point, we start by launching the middleware, then all the required Genom3 components.

To do so, we can use the bash script `launch_genom_pocolibs.sh` that is provided within the material of this tutorial.

**NB:** As you can guess from its filename, we are going to use `pocoLibs` as middleware within this tutorial.

Before running this script, we can inspect its content. Thus, in the terminal we type:

```bash
$ cat launch_genom_pocolibs.sh
#!/bin/bash

# Start pocolibs
h2 init

# Start genomix
genomixd &

# Launch Genom components for pocolibs
optitrack-pocolibs &
rotorcraft-pocolibs &
pom-pocolibs &
nhfc-pocolibs &
maneuver-pocolibs &
```

As in the [previous tutorial](./../quad_gazebo/quad_gazebo.md), we lunch the middleware, the genomix deamon server and the required Genom3 components.

**NB:** the components are the same ones used for the quad-rotor simulation!
As we will see later on, the components allow controlling a generic multi-rotor aerial vehicle. Henceforth, the values of the parameters we use when configuring the Genom3 components determine the type of flying robot (e.g., quad- or hexa-rotor) and its (geometrical, inertial, ...) properties.

As before, we can run this script by typing the following commands:

```bash
$ chmod +x launch_genom_pocolibs.sh
$ ./launch_genom_pocolibs.sh
Initializing pocolibs devices: OK
optitrack: created outport optitrack/genom_state
optitrack: created outport optitrack/genom_metadata
optitrack: spawned task publish
optitrack: spawned control task
optitrack: setup and running
nhfc: created outport nhfc/genom_state
nhfc: created outport nhfc/genom_metadata
pom: created outport pom/genom_state
maneuver: created outport maneuver/genom_state
rotorcraft: created outport rotorcraft/genom_state
nhfc: created outport nhfc/rotor_input
pom: created outport pom/genom_metadata
nhfc: created inport nhfc/rotor_measure
nhfc: created inport nhfc/state
nhfc: created inport nhfc/reference
pom: spawned task io
maneuver: created outport maneuver/genom_metadata
rotorcraft: created outport rotorcraft/genom_metadata
maneuver: created inport maneuver/state
rotorcraft: created inport rotorcraft/rotor_input
nhfc: created outport nhfc/wrench_measure
maneuver: created outport maneuver/desired
rotorcraft: created outport rotorcraft/rotor_measure
nhfc: spawned task main
rotorcraft: created outport rotorcraft/imu
maneuver: created outport maneuver/horizon
rotorcraft: created outport rotorcraft/mag
rotorcraft: spawned task main
maneuver: spawned task plan
maneuver: spawned task exec
pom: spawned task filter
nhfc: spawned control task
nhfc: setup and running
rotorcraft: spawned task comm
rotorcraft: spawned control task
rotorcraft: setup and running
pom: created outport pom/frame/robot
pom: spawned control task
pom: setup and running
maneuver: spawned control task
maneuver: setup and running
```

### 2.2. Gazebo simulator

At this point, we can run the simulator `Gazebo`, which would allow simulating the robot we will control.

We can use the same world file used in the previous tutorial; we recall that there was also an hexa-rotor in the scene!

Therefore, in another terminal, we type:

```bash
$ gazebo --verbose $ROBOTPKG_BASE/share/gazebo/worlds/example.world
```

**In this tutorial, we will control the hexa-rotor on the right.**

### 2.3. Python script

In this tutotial, we will use again a Python script employing the `python-genomix` interface to configure and connect the Genom3 components.
This time the Python script is named `sim_hexa.py`.

Lets have a look to the content of the Python script `sim_hexa.py`, which is reported below for convenience.

```python
################ LIBRARY IMPORT
import genomix
import os


################ INITIALIZATION
# this connects to components running on the same host (localhost)
g = genomix.connect()
# to instead control components running on the remote computer "hostname" use
# g = genomix.connect('hostname')

# adapt path to your setup
g.rpath(os.environ['ROBOTPKG_BASE'] + '/lib/genom/pocolibs/plugins')

# load components clients
optitrack = g.load('optitrack') # motion capture
rotorcraft = g.load('rotorcraft') # robot hardware interface
pom = g.load('pom') # state estimator
nhfc = g.load('nhfc') # attitude-position controller
maneuver = g.load('maneuver') # motion planner


################ FUNCTIONS
# --- setup ----------------------------------------------------------------
#
# Configure the Genom3 components.
# To be called interactively.
def setup():
  ######## optitrack
  #
  # connect to the simulated optitrack system on localhost
  optitrack.connect({
    'host': 'localhost', 'host_port': '1509', 'mcast': '', 'mcast_port': '0'
  })

  ######## rotorcraft
  #
  # connect to the simulated hexarotor
  rotorcraft.connect({'serial': '/tmp/pty-hr6', 'baud': 0})
  #
  # get IMU at 1kHz and motor data at 20Hz
  rotorcraft.set_sensor_rate({'rate': {
    'imu': 1000, 'mag': 0, 'motor': 20, 'battery': 1
  }})
  #
  # Filter IMU: 20Hz cut-off frequency for gyroscopes and 5Hz for
  # accelerometers. This is important for cancelling vibrations.
  rotorcraft.set_imu_filter({
    'gfc': [20, 20, 20], 'afc': [5, 5, 5], 'mfc': [20, 20, 20]
  })
  #
  # read propellers velocities from nhfc controller
  rotorcraft.connect_port({
    'local': 'rotor_input', 'remote': 'nhfc/rotor_input'
  })

  ######## pom
  #
  # configure kalman filter
  pom.set_prediction_model('::pom::constant_acceleration')
  pom.set_process_noise({'max_jerk': 100, 'max_dw': 50})
  #
  # allow sensor data up to 250ms old
  pom.set_history_length({'history_length': 0.25})
  #
  # configure magnetic field
  pom.set_mag_field({'magdir': {
    'x': 23.8e-06, 'y': -0.4e-06, 'z': -39.8e-06
  }})
  #
  # read IMU and magnetometers from rotorcraft
  pom.connect_port({'local': 'measure/imu', 'remote': 'rotorcraft/imu'})
  pom.add_measurement('imu')
  pom.connect_port({'local': 'measure/mag', 'remote': 'rotorcraft/mag'})
  pom.add_measurement('mag')
  #
  # read position and orientation from optitrack
  pom.connect_port({
    'local': 'measure/mocap', 'remote': 'optitrack/bodies/HR_6'
  })
  pom.add_measurement('mocap')

  ######## nhfc
  #
  # PID tuning
  nhfc.set_saturation({'sat': {'x': 1, 'v': 1, 'ix': 0}})
  nhfc.set_servo_gain({ 'gain': {
      'Kpxy': 5, 'Kpz': 5, 'Kqxy': 4, 'Kqz': 4,
      'Kvxy': 6, 'Kvz': 6, 'Kwxy': 1, 'Kwz': 1,
      'Kixy': 0, 'Kiz': 0
  }})
  #
  # use full-attitude controller
  nhfc.set_control_mode({'att_mode': '::nhfc::full_attitude'})
  # set minimum and maximum propeller velocities
  nhfc.set_wlimit({
    'wmin': 16, 'wmax': 100
  })
  #
  # configure hexarotor geometry: 6 rotors, tilted, ~39cm arms
  nhfc.set_gtmrp_geom({
      'rotors': 6, 'cx': 0, 'cy': 0, 'cz': 0, 'armlen': 0.38998, 'mass': 2.72,
      'rx':-20, 'ry': -20, 'rz': -1, 'cf': 9.9016e-4, 'ct': 1.9e-5
  })
  #
  # emergency descent parameters
  nhfc.set_emerg({'emerg': {
      'descent': 0.1, 'dx': 0.5, 'dq': 1, 'dv': 3, 'dw': 3
  }})
  #
  # read measured propeller velocities from rotorcraft
  nhfc.connect_port({
      'local': 'rotor_measure', 'remote': 'rotorcraft/rotor_measure'
  })
  #
  # read current state from pom
  nhfc.connect_port({
      'local': 'state', 'remote': 'pom/frame/robot'
  })
  #
  # read reference trajectory from maneuver
  nhfc.connect_port({
      'local': 'reference', 'remote': 'maneuver/desired'
  })

  ######## maneuver
  #
  # set planning bounds
  pi = 3.14159265358
  maneuver.set_bounds({
    'xmin': -10, 'xmax': +10, 'ymin': -10, 'ymax': +10, 
    'zmin': 0, 'zmax': +10, 'yawmin': -2*pi, 'yawmax': +2*pi
  })
  # configure kalman filter
  maneuver.connect_port({
    'local': 'state', 'remote': 'pom/frame/robot'
  })


# --- start ----------------------------------------------------------------
#
# Spin the motors and servo on current position. 
# To be called interactively.
def start():
  # Start logging the components
  optitrack.set_logfile('/tmp/optitrack.log')
  rotorcraft.log('/tmp/rotorcraft.log')
  pom.log_state('/tmp/pom.log')
  pom.log_measurements('/tmp/pom-measurements.log')
  nhfc.log('/tmp/nhfc.log')
  maneuver.log('/tmp/maneuver.log')
  
  rotorcraft.start() # start spinning the motors
  rotorcraft.servo(ack=True) # this runs until stopped or input error
  nhfc.set_current_position() # hover on current position


# --- takeoff --------------------------------------------------------------
#
# Enable motion planner and start 5s taking-off motion up to 1m height.
# To be called interactively.
def takeoff():
  maneuver.set_current_state() # set initial planning position to current one
  maneuver.take_off(1.0, 3, ack=True) # this runs until the trajectory has been fully generated
  nhfc.servo(ack=True) # this runs untill stopped or input error


# --- square_trajectory ----------------------------------------------------
#
# Generate a square-shaped trajectory made of 4 waypoints.
# To be called interactively.
def square_trajectory():
  pi=3.141592653589
  maneuver.goto(1,2,1,pi/2,0,ack=True) # move to waypoint
  maneuver.waypoint(-1,2,1,pi,0,0,0,0,0,0,0,0,ack=True) # push a waypoint to be reached after the last one
  maneuver.waypoint(-1,0,1,1.5*pi,0,0,0,0,0,0,0,0,ack=True)
  maneuver.waypoint(1,0,1,2*pi,0,0,0,0,0,0,0,0,ack=True)


# --- land -----------------------------------------------------------------
#
# Land to ground in 5s at the current position
# To be called interactively.
def land():
  maneuver.take_off(0.05, 3, ack=True) # this runs until the trajectory has been fully generated


# --- stop -----------------------------------------------------------------
#
# Stop motors. To be called interactively
# To be called interactively.
def stop():
  # Stop the robot
  maneuver.stop() # stop any motion
  nhfc.stop() # stop tracking any reference position and attitude
  rotorcraft.stop() # stop motors
  
  # Stop logging the components
  optitrack.unset_logfile()
  rotorcraft.log_stop()
  pom.log_stop()
  nhfc.log_stop()
  maneuver.log_stop()
```

As we can see, we have the same structure of the code used for the quad-rotor simulation. Indeed, after importing the required libraries, we have an initialization part and then a definition of a set of functions.

The libraries imported are the same, thus we can avoid looking at this part.

Lets have a look at the initialization part to look for the differences with respect to the previous tutorial.
Here it is the content of that section:

```python
################ INITIALIZATION
# this connects to components running on the same host (localhost)
g = genomix.connect()
# to instead control components running on the remote computer "hostname" use
# g = genomix.connect('hostname')

# adapt path to your setup
g.rpath(os.environ['ROBOTPKG_BASE'] + '/lib/genom/pocolibs/plugins')

# load components clients
optitrack = g.load('optitrack') # motion capture
rotorcraft = g.load('rotorcraft') # robot hardware interface
pom = g.load('pom') # state estimator
nhfc = g.load('nhfc') # attitude-position controller
maneuver = g.load('maneuver') # motion planner
```

Also here, there is no difference.

As we are using the same Genom3 components of the quad-rotor simulation, then this part has not changed.
Therefore, we will use the same handles.

Before inspecting the last part of the Python script, we can now run the Python script. Thus, in another terminal, we can execute it interactively [[1]](#ref1) by typing:

```bash
$ python3 -i sim_quad.py
```

## 3. Run the simulation

Hereafter, we will understand how to perform the simulation and fly our hexa-rotor in the Gazebo simulator.

**NB:** In this section, we will focus mainly on the differences in terms of lines of code with respect to the previous tutorial (the quad-rotor simulation).

### 3.1. Set up the hexa-rotor

Similarly to the previous simulation, we need to set up our robot (hexa-rotor).

To do that, in the code, we can find again a function named `setup`, which contains the following lines of code:

```python
# --- setup ----------------------------------------------------------------
#
# Configure the Genom3 components.
# To be called interactively.
def setup():
  ######## optitrack
  #
  # connect to the simulated optitrack system on localhost
  optitrack.connect({
    'host': 'localhost', 'host_port': '1509', 'mcast': '', 'mcast_port': '0'
  })

  ######## rotorcraft
  #
  # connect to the simulated hexarotor
  rotorcraft.connect({'serial': '/tmp/pty-hr6', 'baud': 0})
  #
  # get IMU at 1kHz and motor data at 20Hz
  rotorcraft.set_sensor_rate({'rate': {
    'imu': 1000, 'mag': 0, 'motor': 20, 'battery': 1
  }})
  #
  # Filter IMU: 20Hz cut-off frequency for gyroscopes and 5Hz for
  # accelerometers. This is important for cancelling vibrations.
  rotorcraft.set_imu_filter({
    'gfc': [20, 20, 20], 'afc': [5, 5, 5], 'mfc': [20, 20, 20]
  })
  #
  # read propellers velocities from nhfc controller
  rotorcraft.connect_port({
    'local': 'rotor_input', 'remote': 'nhfc/rotor_input'
  })

  ######## pom
  #
  # configure kalman filter
  pom.set_prediction_model('::pom::constant_acceleration')
  pom.set_process_noise({'max_jerk': 100, 'max_dw': 50})
  #
  # allow sensor data up to 250ms old
  pom.set_history_length({'history_length': 0.25})
  #
  # configure magnetic field
  pom.set_mag_field({'magdir': {
    'x': 23.8e-06, 'y': -0.4e-06, 'z': -39.8e-06
  }})
  #
  # read IMU and magnetometers from rotorcraft
  pom.connect_port({'local': 'measure/imu', 'remote': 'rotorcraft/imu'})
  pom.add_measurement('imu')
  pom.connect_port({'local': 'measure/mag', 'remote': 'rotorcraft/mag'})
  pom.add_measurement('mag')
  #
  # read position and orientation from optitrack
  pom.connect_port({
    'local': 'measure/mocap', 'remote': 'optitrack/bodies/HR_6'
  })
  pom.add_measurement('mocap')

  ######## nhfc
  #
  # PID tuning
  nhfc.set_saturation({'sat': {'x': 1, 'v': 1, 'ix': 0}})
  nhfc.set_servo_gain({ 'gain': {
      'Kpxy': 5, 'Kpz': 5, 'Kqxy': 4, 'Kqz': 4,
      'Kvxy': 6, 'Kvz': 6, 'Kwxy': 1, 'Kwz': 1,
      'Kixy': 0, 'Kiz': 0
  }})
  #
  # use full-attitude controller
  nhfc.set_control_mode({'att_mode': '::nhfc::full_attitude'})
  # set minimum and maximum propeller velocities
  nhfc.set_wlimit({
    'wmin': 16, 'wmax': 100
  })
  #
  # configure hexarotor geometry: 6 rotors, tilted, ~39cm arms
  nhfc.set_gtmrp_geom({
      'rotors': 6, 'cx': 0, 'cy': 0, 'cz': 0, 'armlen': 0.38998, 'mass': 2.72,
      'rx':-20, 'ry': -20, 'rz': -1, 'cf': 9.9016e-4, 'ct': 1.9e-5
  })
  #
  # emergency descent parameters
  nhfc.set_emerg({'emerg': {
      'descent': 0.1, 'dx': 0.5, 'dq': 1, 'dv': 3, 'dw': 3
  }})
  #
  # read measured propeller velocities from rotorcraft
  nhfc.connect_port({
      'local': 'rotor_measure', 'remote': 'rotorcraft/rotor_measure'
  })
  #
  # read current state from pom
  nhfc.connect_port({
      'local': 'state', 'remote': 'pom/frame/robot'
  })
  #
  # read reference trajectory from maneuver
  nhfc.connect_port({
      'local': 'reference', 'remote': 'maneuver/desired'
  })

  ######## maneuver
  #
  # set planning bounds
  pi = 3.14159265358
  maneuver.set_bounds({
    'xmin': -10, 'xmax': +10, 'ymin': -10, 'ymax': +10, 
    'zmin': 0, 'zmax': +10, 'yawmin': -2*pi, 'yawmax': +2*pi
  })
  # configure kalman filter
  maneuver.connect_port({
    'local': 'state', 'remote': 'pom/frame/robot'
  })
```

As we can notice, we have the same lines of codes we run for the quad-rotor case!

Only the following lines are different:

```python
######## rotorcraft
#
# connect to the simulated hexarotor
rotorcraft.connect({'serial': '/tmp/pty-hr6', 'baud': 0})
#
# [...the rest of the code..]
#
######## pom
#
# [...the rest of the code..]
#
# read position and orientation from optitrack
  pom.connect_port({
    'local': 'measure/mocap', 'remote': 'optitrack/bodies/HR_6'
  })
#
# [...the rest of the code..]
#
######## nhfc
#
# [...the rest of the code..]
# configure hexarotor geometry: 6 rotors, tilted, ~39cm arms
nhfc.set_gtmrp_geom({
    'rotors': 6, 'cx': 0, 'cy': 0, 'cz': 0, 'armlen': 0.38998, 'mass': 2.72,
    'rx':-20, 'ry': -20, 'rz': -1, 'cf': 9.9016e-4, 'ct': 1.9e-5
})
```

Basically, the differences are the following:

- As the robot is an hexa-rotor with a different simulated flight-controller, we need to connect to a different serial port, namely `/tmp/pty-hr6`.
- Since we are interested in the state of another robot, from the motion capture, we need to read the state of the hexa-rotor. Thus, the output port we should consider is no more `optitrack/bodies/QR_4`, but rather `optitrack/bodies/HR_6`. The former (`QR_4`) is used by the component `optitrack` to publish the state of the quad-rotor, while the position and attitude (state) of the hexa-rotor is published in the output port ending with `HR_6`.
- As the aerial robot has now 6 rotors (instead of 4), which are tilted with respect to the main body, we need to set different geometrical parameters within the component `nhfc`. Similarly, the mass and the other inertial parameters are different compared to the quad-rotor case.

> In this case, the hexa-rotor has the propellers tilted w.r.t. two axes, respectively, around the `x` axis about `20degrees` and around the `y` axis about `20degrees`. Then, the signs of these rotations for the first propeller are negative, and then alternating signs for the other propellers.

At this point, we can launch the function `setup`. To do that, it is sufficient to type the following command in the Python interpreter command view and press ENTER in your keyboard:

```python
>>> setup()
```

Now, looking at the terminal where we launched the bash script `launch_genom_pocolibs.sh`, we will see the following output:

```bash
optitrack: created outport optitrack/bodies/HR_6
optitrack: created outport optitrack/bodies/QR_4
rotorcraft-pocolibs: connected to chimera1.1, /tmp/pty-hr6
pom: created inport pom/measure/imu
pom: created inport pom/measure/mag
pom: created inport pom/measure/mocap
```

In that terminal output, we can notice that now the component `rotorcraft` successfully connects to the serial port `/tmp/pty-hr6`, which belongs to the hexa-rotor.

### 3.2. Start and Take Off

As before, the Genom3 components are now well initialized, properly configured and connected to each other.

It is time to take off and move our robot!

We can start the robot by running the `start` function.

After running this function, the rotors of the robot should start spinning faster and faster, and the quad-rotor will then start hovering at the initial location.

Interestingly, the content of the function `start` has not changed!

Here it is the content of that function:

```python
# --- start ----------------------------------------------------------------
#
# Spin the motors and servo on current position. 
# To be called interactively.
def start():
  # Start logging the components
  optitrack.set_logfile('/tmp/optitrack.log')
  rotorcraft.log('/tmp/rotorcraft.log')
  pom.log_state('/tmp/pom.log')
  pom.log_measurements('/tmp/pom-measurements.log')
  nhfc.log('/tmp/nhfc.log')
  maneuver.log('/tmp/maneuver.log')
  
  rotorcraft.start() # start spinning the motors
  rotorcraft.servo(ack=True) # this runs until stopped or input error
  nhfc.set_current_position() # hover on current position
```

It is identical to the function `start` used for the quad-rotor simulation. Indeed, we start the logging activity of each component, then we start rotorcraft, as well as the `servo` activities of `rotorcraft` and `nhfc`.

To let the hexa-rotor take off, we can run the function `takeoff`, which should let our robot move vertically up to 1m height.

If we inspect also the content of this function, we will notice the exact same lines of code used for the quad-rotor simulation! Also here, nothing has changed! Nice!

```python
# --- takeoff --------------------------------------------------------------
#
# Enable motion planner and start 5s taking-off motion up to 1m height.
# To be called interactively.
def takeoff():
  maneuver.set_current_state() # set initial planning position to current one
  maneuver.take_off(1.0, 3, ack=True) # this runs until the trajectory has been fully generated
  nhfc.servo(ack=True) # this runs untill stopped or input error
```

Once launched the `takeoff` function, the robot should move vertically as shown in the image below.

![gazebo_hexa_takeoff](./imgs/gazebo_hexa_takeoff.jpg)

### 3.3. Waypoint sequence

At this point, we can ask the robot to reach a desired waypoint again by means of the component `maneuver`.

In the provided Python script, we can find again the function `square_trajectory` which showcases a similar demo scenario as the one of the quad-rotor.
As earlier, this function instructs `maneuver` to generate a trajectory across 4 waypoints forming a square.

After calling this function in the Python interpreter command-line view, the robot should start moving in the environment, as shown in the next figure.

![gazebo_hexa_trajectory](./imgs/gazebo_hexa_trajectory.jpg)

If we inspect the content of the function `square_trajectory`, we will notice that the only changes are related to the coordinates of the first and last waypoints. These two differ as the robot shall start and stop at a different location than the quad-rotor (otherwise, they will crash one into another when landing the hexa-rotor).

```python
# --- square_trajectory ----------------------------------------------------
#
# Generate a square-shaped trajectory made of 4 waypoints.
# To be called interactively.
def square_trajectory():
  pi=3.141592653589
  maneuver.goto(1,2,1,pi/2,0,ack=True) # move to waypoint
  maneuver.waypoint(-1,2,1,pi,0,0,0,0,0,0,0,0,ack=True) # push a waypoint to be reached after the last one
  maneuver.waypoint(-1,0,1,1.5*pi,0,0,0,0,0,0,0,0,ack=True)
  maneuver.waypoint(1,0,1,2*pi,0,0,0,0,0,0,0,0,ack=True)
```

### 3.4. Land and Stop

After completing the trajectory, the robot should have returned to the starting location, and there stop moving.

Now, we can make the robot land by using again the function `land`.
Once the robot is on the ground, we can stop its motors by running the function `stop`.

In these two functions, nothing has changed.

### 3.5. Move the robot to other waypoints

Once the robot has taken off, also in this case, we can move the hexa-rotor by manually calling the service `goto` of `maneuver`.

Therefore, we can use the command:

```python
  maneuver.goto({
    x=<?> , y=<?>, z=<?>, yaw=<?>, duration=<?>, ack=True
  })
```

where `<?>` shall be replaced with a desired value.

#### Example of manual waypoint request

```python
# if not already done, run 
> setup() # calling this function multiple times is not a problem
> start() # propellers should start spinning faster and faster
> takeoff() # robot starts moving upwards
# wait for the robot to at the take-off height, then run
> maneuver.goto(5, 5, 2, 3.14, 0, ack=True)
# this will produce a minimum-time motion to the desired waypoint
# once the waypoint is reached, try running
> maneuver.goto(1, 0, 1, 0, 0, ack=True)
# this will make the robot go back to the starting location
# once there, run
> land()
# wait for landing, then run
> stop()
```

### 3.6. Kill the Genom3 components and stop middleware

As in the previous tutorial, to stop the Genom3 components and the middleware (in this case, `pocoLibs`), a bash script is provided, namely `kill_genom_pocolibs.sh`.

Therefore, we can simply type `./kill_genom_pocolibs.sh` in a terminal to stop the processes related to the Genom3 components, the genomix deamon server, and terminate the middleware.

## 4. References

[1] [Python interpreter in interactive mode](https://docs.python.org/3/tutorial/interpreter.html#interactive-mode). <a name="ref1"></a>

## 5. What is next?

The component `nhfc` has been used so far to control either the quad-rotor or the hexa-rotor.
However, this controller is meant for _under-actuated_ robots, as it does not exploit the tilting of the propellers, if present as in the case of the controlled hexa-rotor.

Indeed, the tilting of the propellers provide the hexa-rotor with the property of **fully-actuation**, i.e., the capability of moving laterally without tilting as a quad-rotor (or, more in general, an under-actuated platform) would do.
Formally speaking, this motion capability implies that the linear dynamics is decoupled from the rotational one.

In the [next tutorial](./../fa_hexa_gazebo/fa_hexa_gazebo.md), we will see how to use the **fully-actuation** of the hexa-rotor, and in particular which are the Genom3 components to be used.
