# Fully-actuated hexa-rotor simulation in Gazebo

[[_TOC_]]

## 1. Introduction

In this tutorial, we will run again a simulation of an hexa-rotor within the Gazebo simulator, but this time we will use its **full actuation**.

Similarly to the [previous tutorial](./../hexa_gazebo/hexa_gazebo.md), the simulation involves the same three following parts:

1. the robot takes off from an initial position,
2. it follows a set of waypoints,
3. lastly, in the last reached waypoint, it lands and stop its motors.

## 2. Set up the simulation

### 2.1. Middleware and Genom3 components

At this point, we start by launching the middleware, then all the required Genom3 components.

To do so, we can use the bash script `launch_genom_pocolibs.sh` that is provided within the material of this tutorial.

**NB:** As you can guess from its filename, we are going to use `pocoLibs` as middleware within this tutorial.

Before running this script, we can inspect its content. Thus, in the terminal we type:

```bash
$ cat launch_genom_pocolibs.sh
#!/bin/bash

# Start pocolibs
h2 init

# Start genomix
genomixd &

# Launch Genom components for pocolibs
optitrack-pocolibs &
rotorcraft-pocolibs &
pom-pocolibs &
uavatt-pocolibs &
uavpos-pocolibs &
maneuver-pocolibs &
```

As in the [previous tutorial](./../hexa_gazebo/hexa_gazebo.md), we lunch the middleware, the genomix deamon server and the required Genom3 components.

**NB:** Now, we have two different components in the list, namely `uavatt` and `uavpos`.
As we will see later on, these two components are the position and attitude controllers for fully-actuated aerial robots.
Indeed, in the previous list, there is no more `nhfc-pocolibs`. As this controller is meant for under-actuated robots, here we do not need it, which will be replaced by the other two components.

As before, we can run this script by typing the following commands:

```bash
$ chmod +x launch_genom_pocolibs.sh
$ ./launch_genom_pocolibs.sh
Initializing pocolibs devices: OK
optitrack: created outport optitrack/genom_state
optitrack: created outport optitrack/genom_metadata
pom: created outport pom/genom_state
optitrack: spawned task publish
pom: created outport pom/genom_metadata
rotorcraft: created outport rotorcraft/genom_state
maneuver: created outport maneuver/genom_state
pom: spawned task io
rotorcraft: created outport rotorcraft/genom_metadata
maneuver: created outport maneuver/genom_metadata
rotorcraft: created inport rotorcraft/rotor_input
maneuver: created inport maneuver/state
rotorcraft: created outport rotorcraft/rotor_measure
maneuver: created outport maneuver/desired
optitrack: spawned control task
rotorcraft: created outport rotorcraft/imu
maneuver: created outport maneuver/horizon
rotorcraft: created outport rotorcraft/mag
rotorcraft: spawned task main
maneuver: spawned task plan
optitrack: setup and running
maneuver: spawned task exec
uavatt: created outport uavatt/genom_state
uavatt: created outport uavatt/genom_metadata
uavatt: created inport uavatt/uav_input
uavatt: created outport uavatt/wrench_measure
uavatt: created outport uavatt/rotor_input
uavatt: created inport uavatt/rotor_measure
uavatt: created inport uavatt/state
uavatt: spawned task main
uavpos: created outport uavpos/genom_state
uavpos: created outport uavpos/genom_metadata
uavpos: created outport uavpos/uav_input
uavpos: created inport uavpos/wrench_measure
uavpos: created inport uavpos/state
uavpos: created inport uavpos/reference
uavpos: spawned task main
rotorcraft: spawned task comm
pom: spawned task filter
rotorcraft: spawned control task
rotorcraft: setup and running
uavatt: spawned control task
uavatt: setup and running
uavpos: spawned control task
uavpos: setup and running
pom: created outport pom/frame/robot
pom: spawned control task
pom: setup and running
maneuver: spawned control task
maneuver: setup and running
```

From the output above, we can see that the two new components (`uavatt` and `uavpos`) are now running.

### 2.2. Gazebo simulator

At this point, we can run the simulator `Gazebo`, which would allow simulating the robot we will control.

We can use the same world file used in the previous tutorial.

Therefore, in another terminal, we type:

```bash
$ gazebo --verbose $ROBOTPKG_BASE/share/gazebo/worlds/example.world
```

**In this tutorial, we will control the hexa-rotor on the right.**

### 2.3. Python script

In this tutotial, we will use again a Python script employing the `python-genomix` interface to configure and connect the Genom3 components.
This time the Python script is named `fa_hexa_gazebo.py`.

Lets have a look to the content of the Python script `fa_hexa_gazebo.py`, which is reported below for convenience.

```python
################ LIBRARY IMPORT
import genomix
import os


################ INITIALIZATION
# this connects to components running on the same host (localhost)
g = genomix.connect()
# to instead control components running on the remote computer "hostname" use
# g = genomix.connect('hostname')

# adapt path to your setup
g.rpath(os.environ['ROBOTPKG_BASE'] + '/lib/genom/pocolibs/plugins')

# load components clients
optitrack = g.load('optitrack') # motion capture
rotorcraft = g.load('rotorcraft') # robot hardware interface
pom = g.load('pom') # state estimator
uavatt = g.load('uavatt') # attitude controller
uavpos = g.load('uavpos') # position controller
maneuver = g.load('maneuver') # motion planner


################ FUNCTIONS
# --- setup ----------------------------------------------------------------
#
# Configure the Genom3 components.
# To be called once.
# To be called interactively.
def setup():
  ######## optitrack
  #
  # connect to the simulated optitrack system on localhost
  optitrack.connect({
    'host': 'localhost', 'host_port': '1509', 'mcast': '', 'mcast_port': '0'
  })

  ######## rotorcraft
  #
  # connect to the simulated quadrotor
  rotorcraft.connect({'serial': '/tmp/pty-hr6', 'baud': 0})
  #
  # get IMU at 1kHz and motor data at 20Hz
  rotorcraft.set_sensor_rate({'rate': {
    'imu': 1000, 'mag': 0, 'motor': 20, 'battery': 1
  }})
  #
  # Filter IMU: 20Hz cut-off frequency for gyroscopes and 5Hz for
  # accelerometers. This is important for cancelling vibrations.
  rotorcraft.set_imu_filter({
    'gfc': [20, 20, 20], 'afc': [5, 5, 5], 'mfc': [20, 20, 20]
  })
  #
  # read propellers velocities from nhfc controller
  rotorcraft.connect_port({
    'local': 'rotor_input', 'remote': 'uavatt/rotor_input'
  })

  ######## pom
  #
  # configure kalman filter
  pom.set_prediction_model('::pom::constant_acceleration')
  pom.set_process_noise({'max_jerk': 100, 'max_dw': 50})
  #
  # allow sensor data up to 250ms old
  pom.set_history_length({'history_length': 0.25})
  #
  # configure magnetic field
  pom.set_mag_field({'magdir': {
    'x': 23.8e-06, 'y': -0.4e-06, 'z': -39.8e-06
  }})
  #
  # read IMU and magnetometers from rotorcraft
  pom.connect_port({'local': 'measure/imu', 'remote': 'rotorcraft/imu'})
  pom.add_measurement('imu')
  pom.connect_port({'local': 'measure/mag', 'remote': 'rotorcraft/mag'})
  pom.add_measurement('mag')
  #
  # read position and orientation from optitrack
  pom.connect_port({
    'local': 'measure/mocap', 'remote': 'optitrack/bodies/HR_6'
  })
  pom.add_measurement('mocap')

  ######## uavpos
  #
  # PID tuning
  uavpos.set_saturation({'sat': {'x': 1, 'v': 1, 'ix': 0}})
  uavpos.set_servo_gain({ 'gain': {
      'Kpxy': 4, 'Kpz': 4, 'Kvxy': 1, 'Kvz': 1, 'Kixy': 0, 'Kiz': 0
  }})
  #
  uavpos.set_mass({
      'mass': 2.72
  })
  # set maximum lateral thrust
  uavpos.set_xyradius({
    'rxy': 2
  })
  #
  # emergency hovering parameters
  uavpos.set_emerg({'emerg': {
      'descent': 0.1, 'dx': 0.05, 'dv': 0.2
  }})
  #
  # read current state from pom
  uavpos.connect_port({
      'local': 'state', 'remote': 'pom/frame/robot'
  })
  #
  # read reference trajectory from maneuver
  uavpos.connect_port({
      'local': 'reference', 'remote': 'maneuver/desired'
  })

  ######## uavatt
  #
  # configure quadrotor geometry: 6 rotors, tilted, ~39cm arms
  uavatt.set_gtmrp_geom({
      'rotors': 6, 'cx': 0, 'cy': 0, 'cz': 0, 'armlen': 0.38998, 'mass': 2.72,
      'rx':-20, 'ry': -20, 'rz': -1, 'cf': 9.9016e-4, 'ct': 1.9e-5
  })
  # set minimum and maximum propeller velocities
  uavatt.set_wlimit({
    'wmin': 16, 'wmax': 100
  })
  #
  # PID tuning
  uavatt.set_servo_gain({ 'gain': {
      'Kqxy': 4, 'Kqz': 4, 'Kwxy': 1, 'Kwz': 1,
  }})
  #
  # emergency hovering parameters
  uavatt.set_emerg({'emerg': {
      'dq': 5, 'dw': 20
  }})
  #
  # read positional control output from uavpos
  uavatt.connect_port({
      'local': 'uav_input', 'remote': 'uavpos/uav_input'
  })
  #
  # read measured propeller velocities from rotorcraft
  uavatt.connect_port({
      'local': 'rotor_measure', 'remote': 'rotorcraft/rotor_measure'
  })
  #
  # read current state from pom
  uavatt.connect_port({
      'local': 'state', 'remote': 'pom/frame/robot'
  })


  ######## maneuver
  #
  # set planning bounds
  pi=3.14
  maneuver.set_bounds({
    'xmin': -10, 'xmax': +10, 'ymin': -10, 'ymax': +10, 
    'zmin': 0, 'zmax': +10, 'yawmin': -2*pi, 'yawmax': +2*pi
  })
  # configure kalman filter
  maneuver.connect_port({
    'local': 'state', 'remote': 'pom/frame/robot'
  })


# --- start ----------------------------------------------------------------
#
# Spin the motors and servo on current position. 
# To be called interactively.
def start():
  # Start logging the components
  optitrack.set_logfile('/tmp/optitrack.log')
  rotorcraft.log('/tmp/rotorcraft.log')
  pom.log_state('/tmp/pom.log')
  pom.log_measurements('/tmp/pom-measurements.log')
  uavpos.log('/tmp/uavpos.log')
  uavatt.log('/tmp/uavatt.log')
  maneuver.log('/tmp/maneuver.log')
  
  rotorcraft.start() # start spinning the motors
  rotorcraft.servo(ack=True) # this runs until stopped or input error
  uavpos.set_current_position() # hover on current position
  uavatt.servo(ack=True) # this runs untill stopped or input error


# --- takeoff --------------------------------------------------------------
#
# Enable motion planner and start 5s taking-off motion up to 1m height.
# To be called interactively.
def takeoff():
  maneuver.set_current_state() # set initial planning position to current one
  maneuver.take_off(1.0, 3, ack=True) # this runs until the trajectory has been fully generated  
  uavpos.servo(ack=True) # this runs untill stopped or input error


# --- square_trajectory ----------------------------------------------------
#
# Generate a square-shaped trajectory made of 4 waypoints.
# To be called interactively.
def square_trajectory():
  pi=3.141592653589
  maneuver.goto(1,2,1,pi/2,0,ack=True) # move to waypoint
  maneuver.waypoint(-1,2,1,pi,0,0,0,0,0,0,0,0,ack=True) # push a waypoint to be reached after the last one
  maneuver.waypoint(-1,0,1,1.5*pi,0,0,0,0,0,0,0,0,ack=True)
  maneuver.waypoint(1,0,1,2*pi,0,0,0,0,0,0,0,0,ack=True)


# --- land -----------------------------------------------------------------
#
# Land to ground in 5s at the current position
# To be called interactively.
def land():
  maneuver.take_off(0.05, 3, ack=True) # this runs until the trajectory has been fully generated


# --- stop -----------------------------------------------------------------
#
# Stop motors. To be called interactively
# To be called interactively.
def stop():
  # Stop the robot
  maneuver.stop() # stop any motion
  uavpos.stop() # stop tracking any reference position
  uavatt.stop() # stop tracking any reference attitude
  rotorcraft.stop() # stop motors
  
  # Stop logging the components
  optitrack.unset_logfile()
  rotorcraft.log_stop()
  pom.log_stop()
  uavpos.log_stop()
  uavatt.log_stop()
  maneuver.log_stop()
```

As we can see, we have again the same structure of the code used for the quad-rotor simulation. Indeed, after importing the required libraries, we have an initialization part and then a definition of a set of functions.

The libraries imported are the same, thus we can avoid looking at this part.

Lets have a look at the initialization part to look for the differences with respect to the previous tutorial.
Here it is the content of that section:

```python
################ INITIALIZATION
# this connects to components running on the same host (localhost)
g = genomix.connect()
# to instead control components running on the remote computer "hostname" use
# g = genomix.connect('hostname')

# adapt path to your setup
g.rpath(os.environ['ROBOTPKG_BASE'] + '/lib/genom/pocolibs/plugins')

# load components clients
optitrack = g.load('optitrack') # motion capture
rotorcraft = g.load('rotorcraft') # robot hardware interface
pom = g.load('pom') # state estimator
uavatt = g.load('uavatt') # attitude controller
uavpos = g.load('uavpos') # position controller
maneuver = g.load('maneuver') # motion planner
```

Here, the difference is that we are loading the two new components (`uavatt` and `uavpos`) instead of `nhfc`.

Before inspecting the last part of the Python script, we can now run the Python script. Thus, in another terminal, we can execute it interactively [[1]](#ref1) by typing:

```bash
$ python3 -i sim_quad.py
```

## 3. Run the simulation

Hereafter, we will understand how to perform the simulation and fly our hexa-rotor in the Gazebo simulator.

**NB:** In this section, we will focus mainly on the differences in terms of lines of code with respect to the previous tutorial (the quad-rotor simulation).

### 3.1. Set up the hexa-rotor

Similarly to the previous simulation, we need to set up our robot (hexa-rotor).

To do that, in the code, we can find again a function named `setup`, which contains the following lines of code:

```python
################ FUNCTIONS
# --- setup ----------------------------------------------------------------
#
# Configure the Genom3 components.
# To be called once.
# To be called interactively.
def setup():
  ######## optitrack
  #
  # connect to the simulated optitrack system on localhost
  optitrack.connect({
    'host': 'localhost', 'host_port': '1509', 'mcast': '', 'mcast_port': '0'
  })

  ######## rotorcraft
  #
  # connect to the simulated quadrotor
  rotorcraft.connect({'serial': '/tmp/pty-hr6', 'baud': 0})
  #
  # get IMU at 1kHz and motor data at 20Hz
  rotorcraft.set_sensor_rate({'rate': {
    'imu': 1000, 'mag': 0, 'motor': 20, 'battery': 1
  }})
  #
  # Filter IMU: 20Hz cut-off frequency for gyroscopes and 5Hz for
  # accelerometers. This is important for cancelling vibrations.
  rotorcraft.set_imu_filter({
    'gfc': [20, 20, 20], 'afc': [5, 5, 5], 'mfc': [20, 20, 20]
  })
  #
  # read propellers velocities from nhfc controller
  rotorcraft.connect_port({
    'local': 'rotor_input', 'remote': 'uavatt/rotor_input'
  })

  ######## pom
  #
  # configure kalman filter
  pom.set_prediction_model('::pom::constant_acceleration')
  pom.set_process_noise({'max_jerk': 100, 'max_dw': 50})
  #
  # allow sensor data up to 250ms old
  pom.set_history_length({'history_length': 0.25})
  #
  # configure magnetic field
  pom.set_mag_field({'magdir': {
    'x': 23.8e-06, 'y': -0.4e-06, 'z': -39.8e-06
  }})
  #
  # read IMU and magnetometers from rotorcraft
  pom.connect_port({'local': 'measure/imu', 'remote': 'rotorcraft/imu'})
  pom.add_measurement('imu')
  pom.connect_port({'local': 'measure/mag', 'remote': 'rotorcraft/mag'})
  pom.add_measurement('mag')
  #
  # read position and orientation from optitrack
  pom.connect_port({
    'local': 'measure/mocap', 'remote': 'optitrack/bodies/HR_6'
  })
  pom.add_measurement('mocap')

  ######## uavpos
  #
  # PID tuning
  uavpos.set_saturation({'sat': {'x': 1, 'v': 1, 'ix': 0}})
  uavpos.set_servo_gain({ 'gain': {
      'Kpxy': 4, 'Kpz': 4, 'Kvxy': 1, 'Kvz': 1, 'Kixy': 0, 'Kiz': 0
  }})
  #
  uavpos.set_mass({
      'mass': 2.72
  })
  # set maximum lateral thrust
  uavpos.set_xyradius({
    'rxy': 2
  })
  #
  # emergency hovering parameters
  uavpos.set_emerg({'emerg': {
      'descent': 0.1, 'dx': 0.05, 'dv': 0.2
  }})
  #
  # read current state from pom
  uavpos.connect_port({
      'local': 'state', 'remote': 'pom/frame/robot'
  })
  #
  # read reference trajectory from maneuver
  uavpos.connect_port({
      'local': 'reference', 'remote': 'maneuver/desired'
  })

  ######## uavatt
  #
  # configure quadrotor geometry: 6 rotors, tilted, ~39cm arms
  uavatt.set_gtmrp_geom({
      'rotors': 6, 'cx': 0, 'cy': 0, 'cz': 0, 'armlen': 0.38998, 'mass': 2.72,
      'rx':-20, 'ry': -20, 'rz': -1, 'cf': 9.9016e-4, 'ct': 1.9e-5
  })
  # set minimum and maximum propeller velocities
  uavatt.set_wlimit({
    'wmin': 16, 'wmax': 100
  })
  #
  # PID tuning
  uavatt.set_servo_gain({ 'gain': {
      'Kqxy': 4, 'Kqz': 4, 'Kwxy': 1, 'Kwz': 1,
  }})
  #
  # emergency hovering parameters
  uavatt.set_emerg({'emerg': {
      'dq': 5, 'dw': 20
  }})
  #
  # read positional control output from uavpos
  uavatt.connect_port({
      'local': 'uav_input', 'remote': 'uavpos/uav_input'
  })
  #
  # read measured propeller velocities from rotorcraft
  uavatt.connect_port({
      'local': 'rotor_measure', 'remote': 'rotorcraft/rotor_measure'
  })
  #
  # read current state from pom
  uavatt.connect_port({
      'local': 'state', 'remote': 'pom/frame/robot'
  })


  ######## maneuver
  #
  # set planning bounds
  pi=3.14
  maneuver.set_bounds({
    'xmin': -10, 'xmax': +10, 'ymin': -10, 'ymax': +10, 
    'zmin': 0, 'zmax': +10, 'yawmin': -2*pi, 'yawmax': +2*pi
  })
  # configure kalman filter
  maneuver.connect_port({
    'local': 'state', 'remote': 'pom/frame/robot'
  })
```

As we can notice, we have the same lines of codes we had for the hexa-rotor case, except for those related to `nhfc`.
Indeed, instead of initializing that component, here we are dealing with the initialization and port connections of `uavatt` and `uavpos`.
Therefore, the different parts of the code are:

```python
#
# [...the rest of the code..]
#
######## rotorcraft
#
# [...the rest of the code..]
#
# read propellers velocities from nhfc controller
rotorcraft.connect_port({
  'local': 'rotor_input', 'remote': 'uavatt/rotor_input'
})
#
# [...the rest of the code..]
#
######## uavpos
#
# PID tuning
uavpos.set_saturation({'sat': {'x': 1, 'v': 1, 'ix': 0}})
uavpos.set_servo_gain({ 'gain': {
    'Kpxy': 4, 'Kpz': 4, 'Kvxy': 1, 'Kvz': 1, 'Kixy': 0, 'Kiz': 0
}})
#
uavpos.set_mass({
    'mass': 2.72
})
# set maximum lateral thrust
uavpos.set_xyradius({
  'rxy': 2
})
#
# emergency hovering parameters
uavpos.set_emerg({'emerg': {
    'descent': 0.1, 'dx': 0.05, 'dv': 0.2
}})
#
# read current state from pom
uavpos.connect_port({
    'local': 'state', 'remote': 'pom/frame/robot'
})
#
# read reference trajectory from maneuver
uavpos.connect_port({
    'local': 'reference', 'remote': 'maneuver/desired'
})

######## uavatt
#
# configure quadrotor geometry: 6 rotors, tilted, ~39cm arms
uavatt.set_gtmrp_geom({
    'rotors': 6, 'cx': 0, 'cy': 0, 'cz': 0, 'armlen': 0.38998, 'mass': 2.72,
    'rx':-20, 'ry': -20, 'rz': -1, 'cf': 9.9016e-4, 'ct': 1.9e-5
})
# set minimum and maximum propeller velocities
uavatt.set_wlimit({
  'wmin': 16, 'wmax': 100
})
#
# PID tuning
uavatt.set_servo_gain({ 'gain': {
    'Kqxy': 4, 'Kqz': 4, 'Kwxy': 1, 'Kwz': 1,
}})
#
# emergency hovering parameters
uavatt.set_emerg({'emerg': {
    'dq': 5, 'dw': 20
}})
#
# read positional control output from uavpos
uavatt.connect_port({
    'local': 'uav_input', 'remote': 'uavpos/uav_input'
})
#
# read measured propeller velocities from rotorcraft
uavatt.connect_port({
    'local': 'rotor_measure', 'remote': 'rotorcraft/rotor_measure'
})
#
# read current state from pom
uavatt.connect_port({
    'local': 'state', 'remote': 'pom/frame/robot'
})
```

Basically, the differences are the following:

- As we are using new robot controllers, we need to connect the input port `rotor_input` of `rotorcraft` to the new component generating the propeller velocity commands, i.e., `uavatt`.
- As we are using now `uavatt` and `uavpos`, we need to initialize their parameters and connect their input ports.
**NB:** the output of the positional loop (i.e., the output port `uavpos/uav_input`) shall be connected to the input port `uav_input` of `uavatt`.

At this point, we can launch the function `setup`. To do that, it is sufficient to type the following command in the Python interpreter command view and press ENTER in your keyboard:

```python
>>> setup()
```

### 3.2. Start and Take Off

As before, the Genom3 components are now well initialized, properly configured and connected to each other.

It is time to take off and move our robot!

We can start the robot by running the `start` function.

After running this function, the rotors of the robot should start spinning faster and faster, and the quad-rotor will then start hovering at the initial location.

We can now have a look at the content of this function, which is shown below.

```python
# --- start ----------------------------------------------------------------
#
# Spin the motors and servo on current position. 
# To be called interactively.
def start():
  # Start logging the components
  optitrack.set_logfile('/tmp/optitrack.log')
  rotorcraft.log('/tmp/rotorcraft.log')
  pom.log_state('/tmp/pom.log')
  pom.log_measurements('/tmp/pom-measurements.log')
  uavpos.log('/tmp/uavpos.log')
  uavatt.log('/tmp/uavatt.log')
  maneuver.log('/tmp/maneuver.log')
  
  rotorcraft.start() # start spinning the motors
  rotorcraft.servo(ack=True) # this runs until stopped or input error
  uavpos.set_current_position() # hover on current position
  uavatt.servo(ack=True) # this runs untill stopped or input error
```

These lines of code are very similar to the ones of the previous simulation with the hexa-rotor. However, since now we are using `uavatt` and `uavpos`, we call their logging services and we use run the `servo` activity of `uavatt`. Additionally, we call the service `set_current_position` of `uavpos` as this is used to have the positional loop stabilize the platform the platform so that the current position is hold. Therefore, with this line, we can make the hexa-rotor hover in the current position (the starting one).

To let the hexa-rotor take off, we can run the function `takeoff`, which should let our robot move vertically up to 1m height.

Lets have a look also to its content:

```python
# --- takeoff --------------------------------------------------------------
#
# Enable motion planner and start 5s taking-off motion up to 1m height.
# To be called interactively.
def takeoff():
  maneuver.set_current_state() # set initial planning position to current one
  maneuver.take_off(1.0, 3, ack=True) # this runs until the trajectory has been fully generated  
  uavpos.servo(ack=True) # this runs untill stopped or input error
```

As done previously, we call the `set_current_state` and `take_off` services of `maneuver` to let it publish a vertical take-off trajectory from the robot starting position.
Then, we need also to start the `servo` activity of `uavpos`. Indeed, as now we have two controllers, we need to run this same activity for `uavatt` and `uavpos`. While `uavatt` was already _"servoing"_ after calling the `start` function, `uavpos` was not yet _"servoing"_ on the reference trajectory coming from maneuver.

Once launched the `takeoff` function, the robot should move vertically.

### 3.3. Waypoint sequence

At this point, we can ask the robot to reach a desired waypoint again by means of the component `maneuver`.

In the provided Python script, we can find again the function `square_trajectory` which showcases a similar demo scenario as the one seen for the previous hexa-rotor tutorial.
As earlier, this function instructs `maneuver` to generate a trajectory across 4 waypoints forming a square.

After calling this function in the Python interpreter command-line view, the robot should start moving in the environment, as shown in the next figure.

![gazebo_fahexa_trajectory](./imgs/gazebo_fahexa_trajectory.jpg)

**NB:** _"What has changed?"_ Basically, not much...but interestingly, the robot now has moved keeping almost a _flat_ attitude. Indeed, differently from the quad-rotor case and the hexa-rotor of the previous tutorial, the robot has slided without changing its attitude.

This is only possible thanks to the capability of this kind of platforms (_fully-actuated_ aerial robots) to generate lateral forces wrt their body frame.

In Telekyb3, this ability is exploited by the controller `uavatt` and `uavpos` which, under some limits (see the service [set_xyradius](https://git.openrobots.org/projects/uavpos-genom3/pages/README#set_xyradius)) of `uavpos`, can command lateral forces.

### 3.4. Land and Stop

After completing the trajectory, the robot should have returned to the starting location, and there stop moving.

Now, we can make the robot land by using again the function `land`.
Once the robot is on the ground, we can stop its motors by running the function `stop`.

In the `land` function, nothing has changed.

However, the content of the `stop` one has been slightly modified as follows:

```python
# --- stop -----------------------------------------------------------------
#
# Stop motors. To be called interactively
# To be called interactively.
def stop():
  # Stop the robot
  maneuver.stop() # stop any motion
  uavpos.stop() # stop tracking any reference position
  uavatt.stop() # stop tracking any reference attitude
  rotorcraft.stop() # stop motors
  
  # Stop logging the components
  optitrack.unset_logfile()
  rotorcraft.log_stop()
  pom.log_stop()
  uavpos.log_stop()
  uavatt.log_stop()
  maneuver.log_stop()
```

Similarly to the `start` function, we need now to _stop_ the new controllers, thus `uavatt` and `uavpos`. Similarly, we will need to end their logging activities.

### 3.5. Move the robot to other waypoints

Once the robot has taken off, also in this case, we can move the hexa-rotor by manually calling the service `goto` of `maneuver`.

Therefore, we can use the command:

```python
  maneuver.goto({
    x=<?> , y=<?>, z=<?>, yaw=<?>, duration=<?>, ack=True
  })
```

where `<?>` shall be replaced with a desired value.

#### Example of manual waypoint request

```python
# if not already done, run 
> setup() # calling this function multiple times is not a problem
> start() # propellers should start spinning faster and faster
> takeoff() # robot starts moving upwards
# wait for the robot to at the take-off height, then run
> maneuver.goto(5, 5, 2, 3.14, 0, ack=True)
# this will produce a minimum-time motion to the desired waypoint
# once the waypoint is reached, try running
> maneuver.goto(1, 0, 1, 0, 0, ack=True)
# this will make the robot go back to the starting location
# once there, run
> land()
# wait for landing, then run
> stop()
```

### 3.6. Kill the Genom3 components and stop middleware

As in the previous tutorial, to stop the Genom3 components and the middleware (in this case, `pocoLibs`), a bash script is provided, namely `kill_genom_pocolibs.sh`.

Therefore, we can simply type `./kill_genom_pocolibs.sh` in a terminal to stop the processes related to the Genom3 components, the genomix deamon server, and terminate the middleware.

## 4. References

[1] [Python interpreter in interactive mode](https://docs.python.org/3/tutorial/interpreter.html#interactive-mode). <a name="ref1"></a>
