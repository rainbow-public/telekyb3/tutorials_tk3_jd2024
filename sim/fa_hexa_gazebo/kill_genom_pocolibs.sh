#!/bin/bash

# Kill the processes related to the Genom components
pkill optitrack-pocolibs &
pkill rotorcraft-pocolibs &
pkill pom-pocolibs &
pkill uavatt-pocolibs &
pkill uavpos-pocolibs &
pkill maneuver-pocolibs &

# Kill genomix
pkill genomixd &

# Close pocolibs
h2 end