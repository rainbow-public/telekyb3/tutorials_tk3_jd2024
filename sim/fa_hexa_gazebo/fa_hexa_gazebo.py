################ LIBRARY IMPORT
import genomix
import os


################ INITIALIZATION
# this connects to components running on the same host (localhost)
g = genomix.connect()
# to instead control components running on the remote computer "hostname" use
# g = genomix.connect('hostname')

# adapt path to your setup
g.rpath(os.environ['ROBOTPKG_BASE'] + '/lib/genom/pocolibs/plugins')

# load components clients
optitrack = g.load('optitrack') # motion capture
rotorcraft = g.load('rotorcraft') # robot hardware interface
pom = g.load('pom') # state estimator
uavatt = g.load('uavatt') # attitude controller
uavpos = g.load('uavpos') # position controller
maneuver = g.load('maneuver') # motion planner


################ FUNCTIONS
# --- setup ----------------------------------------------------------------
#
# Configure the Genom3 components.
# To be called once.
# To be called interactively.
def setup():
  ######## optitrack
  #
  # connect to the simulated optitrack system on localhost
  optitrack.connect({
    'host': 'localhost', 'host_port': '1509', 'mcast': '', 'mcast_port': '0'
  })

  ######## rotorcraft
  #
  # connect to the simulated quadrotor
  rotorcraft.connect({'serial': '/tmp/pty-hr6', 'baud': 0})
  #
  # get IMU at 1kHz and motor data at 20Hz
  rotorcraft.set_sensor_rate({'rate': {
    'imu': 1000, 'mag': 0, 'motor': 20, 'battery': 1
  }})
  #
  # Filter IMU: 20Hz cut-off frequency for gyroscopes and 5Hz for
  # accelerometers. This is important for cancelling vibrations.
  rotorcraft.set_imu_filter({
    'gfc': [20, 20, 20], 'afc': [5, 5, 5], 'mfc': [20, 20, 20]
  })
  #
  # read propellers velocities from nhfc controller
  rotorcraft.connect_port({
    'local': 'rotor_input', 'remote': 'uavatt/rotor_input'
  })

  ######## pom
  #
  # configure kalman filter
  pom.set_prediction_model('::pom::constant_acceleration')
  pom.set_process_noise({'max_jerk': 100, 'max_dw': 50})
  #
  # allow sensor data up to 250ms old
  pom.set_history_length({'history_length': 0.25})
  #
  # configure magnetic field
  pom.set_mag_field({'magdir': {
    'x': 23.8e-06, 'y': -0.4e-06, 'z': -39.8e-06
  }})
  #
  # read IMU and magnetometers from rotorcraft
  pom.connect_port({'local': 'measure/imu', 'remote': 'rotorcraft/imu'})
  pom.add_measurement('imu')
  pom.connect_port({'local': 'measure/mag', 'remote': 'rotorcraft/mag'})
  pom.add_measurement('mag')
  #
  # read position and orientation from optitrack
  pom.connect_port({
    'local': 'measure/mocap', 'remote': 'optitrack/bodies/HR_6'
  })
  pom.add_measurement('mocap')

  ######## uavpos
  #
  # PID tuning
  uavpos.set_saturation({'sat': {'x': 1, 'v': 1, 'ix': 0}})
  uavpos.set_servo_gain({ 'gain': {
      'Kpxy': 4, 'Kpz': 4, 'Kvxy': 1, 'Kvz': 1, 'Kixy': 0, 'Kiz': 0
  }})
  #
  uavpos.set_mass({
      'mass': 2.72
  })
  # set maximum lateral thrust
  uavpos.set_xyradius({
    'rxy': 2
  })
  #
  # emergency hovering parameters
  uavpos.set_emerg({'emerg': {
      'descent': 0.1, 'dx': 0.05, 'dv': 0.2
  }})
  #
  # read current state from pom
  uavpos.connect_port({
      'local': 'state', 'remote': 'pom/frame/robot'
  })
  #
  # read reference trajectory from maneuver
  uavpos.connect_port({
      'local': 'reference', 'remote': 'maneuver/desired'
  })

  ######## uavatt
  #
  # configure quadrotor geometry: 6 rotors, tilted, ~39cm arms
  uavatt.set_gtmrp_geom({
      'rotors': 6, 'cx': 0, 'cy': 0, 'cz': 0, 'armlen': 0.38998, 'mass': 2.72,
      'rx':-20, 'ry': -20, 'rz': -1, 'cf': 9.9016e-4, 'ct': 1.9e-5
  })
  # set minimum and maximum propeller velocities
  uavatt.set_wlimit({
    'wmin': 16, 'wmax': 100
  })
  #
  # PID tuning
  uavatt.set_servo_gain({ 'gain': {
      'Kqxy': 4, 'Kqz': 4, 'Kwxy': 1, 'Kwz': 1,
  }})
  #
  # emergency hovering parameters
  uavatt.set_emerg({'emerg': {
      'dq': 5, 'dw': 20
  }})
  #
  # read positional control output from uavpos
  uavatt.connect_port({
      'local': 'uav_input', 'remote': 'uavpos/uav_input'
  })
  #
  # read measured propeller velocities from rotorcraft
  uavatt.connect_port({
      'local': 'rotor_measure', 'remote': 'rotorcraft/rotor_measure'
  })
  #
  # read current state from pom
  uavatt.connect_port({
      'local': 'state', 'remote': 'pom/frame/robot'
  })


  ######## maneuver
  #
  # set planning bounds
  pi=3.14
  maneuver.set_bounds({
    'xmin': -10, 'xmax': +10, 'ymin': -10, 'ymax': +10, 
    'zmin': 0, 'zmax': +10, 'yawmin': -2*pi, 'yawmax': +2*pi
  })
  # configure kalman filter
  maneuver.connect_port({
    'local': 'state', 'remote': 'pom/frame/robot'
  })


# --- start ----------------------------------------------------------------
#
# Spin the motors and servo on current position. 
# To be called interactively.
def start():
  # Start logging the components
  optitrack.set_logfile('/tmp/optitrack.log')
  rotorcraft.log('/tmp/rotorcraft.log')
  pom.log_state('/tmp/pom.log')
  pom.log_measurements('/tmp/pom-measurements.log')
  uavpos.log('/tmp/uavpos.log')
  uavatt.log('/tmp/uavatt.log')
  maneuver.log('/tmp/maneuver.log')
  
  rotorcraft.start() # start spinning the motors
  rotorcraft.servo(ack=True) # this runs until stopped or input error
  uavpos.set_current_position() # hover on current position
  uavatt.servo(ack=True) # this runs untill stopped or input error


# --- takeoff --------------------------------------------------------------
#
# Enable motion planner and start 5s taking-off motion up to 1m height.
# To be called interactively.
def takeoff():
  maneuver.set_current_state() # set initial planning position to current one
  maneuver.take_off(1.0, 3, ack=True) # this runs until the trajectory has been fully generated  
  uavpos.servo(ack=True) # this runs untill stopped or input error


# --- square_trajectory ----------------------------------------------------
#
# Generate a square-shaped trajectory made of 4 waypoints.
# To be called interactively.
def square_trajectory():
  pi=3.141592653589
  maneuver.goto(1,2,1,pi/2,0,ack=True) # move to waypoint
  maneuver.waypoint(-1,2,1,pi,0,0,0,0,0,0,0,0,ack=True) # push a waypoint to be reached after the last one
  maneuver.waypoint(-1,0,1,1.5*pi,0,0,0,0,0,0,0,0,ack=True)
  maneuver.waypoint(1,0,1,2*pi,0,0,0,0,0,0,0,0,ack=True)


# --- land -----------------------------------------------------------------
#
# Land to ground in 5s at the current position
# To be called interactively.
def land():
  maneuver.take_off(0.05, 3, ack=True) # this runs until the trajectory has been fully generated


# --- stop -----------------------------------------------------------------
#
# Stop motors. To be called interactively
# To be called interactively.
def stop():
  # Stop the robot
  maneuver.stop() # stop any motion
  uavpos.stop() # stop tracking any reference position
  uavatt.stop() # stop tracking any reference attitude
  rotorcraft.stop() # stop motors
  
  # Stop logging the components
  optitrack.unset_logfile()
  rotorcraft.log_stop()
  pom.log_stop()
  uavpos.log_stop()
  uavatt.log_stop()
  maneuver.log_stop()