#!/bin/bash

# Start pocolibs
h2 init

# Start genomix
genomixd &

# Launch Genom components for pocolibs
optitrack-pocolibs &
rotorcraft-pocolibs &
pom-pocolibs &
nhfc-pocolibs &
maneuver-pocolibs &