################ LIBRARY IMPORT
import genomix
import json


################ INITIALIZATION
# this connects to components running on the same host (localhost)
g = genomix.connect('10.135.2.51')
# to instead control components running on the remote computer "hostname" use
# g = genomix.connect('hostname')

# adapt path to your setup
g.rpath('/home/mtognon/openrobots' + '/lib/genom/pocolibs/plugins')
g.rpath('/home/mtognon/devel' + '/lib/genom/pocolibs/plugins') # we need to import also qualisys-genom3 which is installed under '/home/mtognon/devel'

# load components clients
qualisys = g.load('qualisys') # motion capture
rotorcraft = g.load('rotorcraft') # robot hardware interface
pom = g.load('pom') # state estimator
uavatt = g.load('uavatt') # attitude controller
uavpos = g.load('uavpos') # position controller
maneuver = g.load('maneuver') # motion planner


################ FUNCTIONS
# --- setup ----------------------------------------------------------------
#
# Configure the Genom3 components.
# To be called once.
# To be called interactively.
def setup():
  ######## qualisys
  #
  # connect to the simulated qualisys system on localhost
  qualisys.connect({
    'host': '10.135.2.39'
  })

  ######## rotorcraft
  #
  # connect to the simulated quadrotor
  rotorcraft.connect({'serial': 'chimera-10', 'baud': 0})
  #
  # get IMU at 1kHz and motor data at 20Hz
  rotorcraft.set_sensor_rate({'rate': {
    'imu': 1000, 'mag': 0, 'motor': 20, 'battery': 1
  }})
  #
  # Filter IMU: 20Hz cut-off frequency for gyroscopes and 5Hz for
  # accelerometers. This is important for cancelling vibrations.
  rotorcraft.set_imu_filter({
    'gfc': [20, 20, 20], 'afc': [5, 5, 5], 'mfc': [20, 20, 20]
  })
  #
  # read propellers velocities from nhfc controller
  rotorcraft.connect_port({
    'local': 'rotor_input', 'remote': 'uavatt/rotor_input'
  })
  # set IMU calibration file
  rc_calib = json.load(open("calibs/20240521_pcHexa1.json"))
  rotorcraft.set_imu_calibration(rc_calib)

  ######## pom
  #
  # configure kalman filter
  pom.set_prediction_model('::pom::constant_acceleration')
  pom.set_process_noise({'max_jerk': 100, 'max_dw': 50})
  #
  # allow sensor data up to 250ms old
  pom.set_history_length({'history_length': 0.25})
  #
  # WE WILL NOT USE THE MAGNETOMETER
  # configure magnetic field
  # pom.set_mag_field({'magdir': {
  #   'x': 23.8e-06, 'y': -0.4e-06, 'z': -39.8e-06
  # }})
  #
  # read IMU and magnetometers from rotorcraft
  pom.connect_port({'local': 'measure/imu', 'remote': 'rotorcraft/imu'})
  pom.add_measurement('imu')
  # WE WILL NOT USE THE MAGNETOMETER
  # pom.connect_port({'local': 'measure/mag', 'remote': 'rotorcraft/mag'})
  # pom.add_measurement('mag')
  #
  # read position and orientation from qualisys
  pom.connect_port({
    'local': 'measure/mocap', 'remote': 'qualisys/bodies/pcHexa1'
  })
  pom.add_measurement('mocap')

  ######## uavpos
  #
  # PID tuning
  uavpos.set_saturation({'sat': {'x': 0.3, 'v': 0.2, 'ix': 0}})
  uavpos.set_servo_gain({ 'gain': {
      'Kpxy': 20, 'Kpz': 20, 'Kvxy': 10, 'Kvz': 10, 'Kixy': 0, 'Kiz': 0
  }})
  #
  uavpos.set_mass({
      'mass': 2.90
  })
  # set maximum lateral thrust
  uavpos.set_xyradius({
    'rxy': 2
  })
  #
  # emergency hovering parameters
  uavpos.set_emerg({'emerg': {
      'descent': 0.5, 'dx': 10, 'dv': 10
  }})
  #
  # read current state from pom
  uavpos.connect_port({
      'local': 'state', 'remote': 'pom/frame/robot'
  })
  #
  # read reference trajectory from maneuver
  uavpos.connect_port({
      'local': 'reference', 'remote': 'maneuver/desired'
  })

  ######## uavatt
  #
  # configure quadrotor geometry: 6 rotors, tilted, ~39cm arms
  uavatt.set_gtmrp_geom({
      'rotors': 6, 'cx': 0, 'cy': 0, 'cz': 0, 'armlen': 0.39, 'mass': 2.90,
      'rx':-20, 'ry': 0, 'rz': -1, 'cf':12.0e-4, 'ct': 2.4e-5
  })
  # set minimum and maximum propeller velocities
  uavatt.set_wlimit({
    'wmin': 16, 'wmax': 100
  })
  #
  # PID tuning
  uavatt.set_servo_gain({ 'gain': {
      'Kqxy': 15, 'Kqz': 15, 'Kwxy': 1.5, 'Kwz': 1.5,
  }})
  #
  # emergency hovering parameters
  uavatt.set_emerg({'emerg': {
      'dq': 10, 'dw': 20
  }})
  #
  # read positional control output from uavpos
  uavatt.connect_port({
      'local': 'uav_input', 'remote': 'uavpos/uav_input'
  })
  #
  # read measured propeller velocities from rotorcraft
  uavatt.connect_port({
      'local': 'rotor_measure', 'remote': 'rotorcraft/rotor_measure'
  })
  #
  # read current state from pom
  uavatt.connect_port({
      'local': 'state', 'remote': 'pom/frame/robot'
  })


  ######## maneuver
  #
  # set planning bounds
  pi=3.14
  maneuver.set_bounds({
    'xmin': -10, 'xmax': +10, 'ymin': -10, 'ymax': +10, 
    'zmin': 0, 'zmax': +10, 'yawmin': -2*pi, 'yawmax': +2*pi
  })
  # configure kalman filter
  maneuver.connect_port({
    'local': 'state', 'remote': 'pom/frame/robot'
  })


# --- start ----------------------------------------------------------------
#
# Spin the motors and servo on current position. 
# To be called interactively.
def start():
  # Start logging the components
  qualisys.set_logfile('/tmp/qualisys.log')
  rotorcraft.log('/tmp/rotorcraft.log')
  pom.log_state('/tmp/pom.log')
  pom.log_measurements('/tmp/pom-measurements.log')
  uavpos.log('/tmp/uavpos.log')
  uavatt.log('/tmp/uavatt.log')
  maneuver.log('/tmp/maneuver.log')
  
  rotorcraft.start() # start spinning the motors
  rotorcraft.servo(ack=True) # this runs until stopped or input error
  uavpos.set_current_position() # hover on current position
  uavatt.servo(ack=True) # this runs untill stopped or input error


# --- takeoff --------------------------------------------------------------
#
# Enable motion planner and start 5s taking-off motion up to 1m height.
# To be called interactively.
def takeoff():
  maneuver.set_current_state() # set initial planning position to current one
  maneuver.take_off(1.0, 6, ack=True) # this runs until the trajectory has been fully generated  
  uavpos.servo(ack=True) # this runs untill stopped or input error


# --- square_trajectory ----------------------------------------------------
#
# Generate a square-shaped trajectory made of 4 waypoints.
# To be called interactively.
def square_trajectory():
  pi=3.1
  # NB: the robot starts in -1,0
  maneuver.goto(0,0,1,pi/2,4,ack=True) # move to waypoint
  maneuver.waypoint(0,-1,1,pi,0,0,0,0,0,0,0,4,ack=True) # push a waypoint to be reached after the last one
  maneuver.waypoint(-1,-1.5,1,1.5*pi,0,0,0,0,0,0,0,4,ack=True)
  maneuver.waypoint(-1,0,1,2*pi,0,0,0,0,0,0,0,4,ack=True)


# --- land -----------------------------------------------------------------
#
# Land to ground in 5s at the current position
# To be called interactively.
def land():
  maneuver.take_off(0.30, 6, ack=True) # this runs until the trajectory has been fully generated


# --- stop -----------------------------------------------------------------
#
# Stop motors. To be called interactively
# To be called interactively.
def stop():
  # Stop the robot
  maneuver.stop() # stop any motion
  uavpos.stop() # stop tracking any reference position
  uavatt.stop() # stop tracking any reference attitude
  rotorcraft.stop() # stop motors
  
  # Stop logging the components
  qualisys.unset_logfile()
  rotorcraft.log_stop()
  pom.log_stop()
  uavpos.log_stop()
  uavatt.log_stop()
  maneuver.log_stop()
